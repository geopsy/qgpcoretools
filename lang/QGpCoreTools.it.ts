<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>AllocatedCacheItem</name>
    <message>
        <location filename="../src/Cache.cpp" line="382"/>
        <source>%1 allocated blocks (%2 Mb), %3 locked blocks (%4 Mb)
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArgumentStdinReader</name>
    <message>
        <location filename="../src/ArgumentStdinReader.cpp" line="66"/>
        <source>Cannot open file %1 for reading.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Cache</name>
    <message>
        <location filename="../src/Cache.cpp" line="243"/>
        <location filename="../src/Cache.cpp" line="295"/>
        <source>Impossible to allocate new data
   current used space: %1 Kb
   current free space: %2 Kb
   current buffer size: %3 Kb
   required space: %4 Kb
Increase buffer size, a good compromize is 80% of you physical memory
This buffer is allocated dynamically upon requests, so it will not overload
your memory usage.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Cache.cpp" line="275"/>
        <source>Impossible to allocate new data of size %1: current used space: %2 Kb.

</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColumnTextParser</name>
    <message>
        <location filename="../src/ColumnTextParser.cpp" line="149"/>
        <source>Skipped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ColumnTextParser.cpp" line="477"/>
        <source>No column index defined for description</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExpressionAction</name>
    <message>
        <location filename="../src/ExpressionAction.cpp" line="393"/>
        <location filename="../src/ExpressionAction.cpp" line="402"/>
        <location filename="../src/ExpressionAction.cpp" line="406"/>
        <location filename="../src/ExpressionAction.cpp" line="416"/>
        <source>Division by zero</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExpressionParser</name>
    <message>
        <location filename="../src/ExpressionParser.cpp" line="106"/>
        <source>Not a valid argument, syntax error %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExpressionParser.cpp" line="121"/>
        <source>Unmatched bracket %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExpressionParser.cpp" line="131"/>
        <source>Square brackets allowed only for arrays: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExpressionParser.cpp" line="136"/>
        <source>Unmatched square bracket %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExpressionParser.cpp" line="145"/>
        <source>No coma expected here %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExpressionParser.cpp" line="161"/>
        <source>Not a valid operator, syntax error %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExpressionParser.cpp" line="376"/>
        <source>Unrecognized character %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExpressionParser.cpp" line="383"/>
        <source>at line %1 and column %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExpressionParser.cpp" line="392"/>
        <source>Unmatched &quot; before reaching the end of buffer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExpressionString</name>
    <message>
        <location filename="../src/ExpressionString.cpp" line="61"/>
        <source>Unmatched { before reaching the end of buffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExpressionString.cpp" line="114"/>
        <source>Unmatched &quot; before reaching the end of buffer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IrregularGrid2D</name>
    <message>
        <location filename="../src/IrregularGrid2D.cpp" line="65"/>
        <source>Wrong format at line 3, expected: x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/IrregularGrid2D.cpp" line="72"/>
        <location filename="../src/IrregularGrid2D.cpp" line="86"/>
        <location filename="../src/IrregularGrid2D.cpp" line="99"/>
        <location filename="../src/IrregularGrid2D.cpp" line="119"/>
        <source>Wrong format at line %1, expected: float value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/IrregularGrid2D.cpp" line="79"/>
        <source>Wrong format at line %1, expected: y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/IrregularGrid2D.cpp" line="108"/>
        <location filename="../src/IrregularGrid2D.cpp" line="164"/>
        <source>Wrong format at line %1, expected: values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/IrregularGrid2D.cpp" line="246"/>
        <source>IrregularGrid2D size in binary file: %1x%2
                     in XML data   : %3x%4</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IrregularGrid2DData</name>
    <message>
        <location filename="../src/IrregularGrid2DData.cpp" line="526"/>
        <source>Error in median(), grid not normalized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/IrregularGrid2DData.cpp" line="588"/>
        <location filename="../src/IrregularGrid2DData.cpp" line="626"/>
        <source>### ERROR ### : The grid is certainly not normalized, you must call normalize()                 before calling variance().</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="../src/Message.cpp" line="136"/>
        <source>-----INFO----- </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="138"/>
        <source>----WARNING--- </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="140"/>
        <source>----ERROR----- </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="142"/>
        <source>--FATAL ERROR- </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="144"/>
        <source>---QUESTION--- </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="146"/>
        <source>-------------- </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="169"/>
        <source> &lt;-- default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="172"/>
        <source>? </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="186"/>
        <source>Show this message again? [y]/n </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="305"/>
        <location filename="../src/Message.cpp" line="320"/>
        <location filename="../src/Message.cpp" line="335"/>
        <source>  Filter: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="306"/>
        <location filename="../src/Message.cpp" line="336"/>
        <source>  Current directory: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="307"/>
        <source>File to open: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="321"/>
        <source>  Selection: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="322"/>
        <source>File to save: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="340"/>
        <source>File %1 to open: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="356"/>
        <source>  Dir: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="357"/>
        <source>Directory to choose: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="371"/>
        <location filename="../src/Message.cpp" line="419"/>
        <source>Error parsing text file %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="373"/>
        <source>Error parsing text
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="379"/>
        <source>Offset from beginning of file: %1 bytes.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="384"/>
        <location filename="../src/Message.cpp" line="397"/>
        <location filename="../src/Message.cpp" line="432"/>
        <location filename="../src/Message.cpp" line="449"/>
        <source>    </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="390"/>
        <location filename="../src/Message.cpp" line="440"/>
        <source>==&gt; %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="402"/>
        <source>Impossible to print the context of the parsing error
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Message.cpp" line="417"/>
        <source>Error parsing text file
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ParallelLoop</name>
    <message>
        <location filename="../src/ParallelLoop.cpp" line="78"/>
        <source>Finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PathTranslator</name>
    <message>
        <location filename="../src/PathTranslator.cpp" line="131"/>
        <source>File %1 does not exist.
The path might have changed. Would you like to manually select its new location?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PathTranslator.cpp" line="145"/>
        <source>Selected file is not the correct file. Aborting.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtApplicationHelp</name>
    <message>
        <location filename="../src/QtApplicationHelp.cpp" line="142"/>
        <source>Usage: %1 %2
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtCoreApplication</name>
    <message>
        <location filename="../src/QtCoreApplication.cpp" line="94"/>
        <source>
Please copy the message below to http://www.geopsy.org/bugs/backtrace.php:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplication.cpp" line="96"/>
        <source>Please copy the above message to http://www.geopsy.org/bugs/backtrace.php.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplication.cpp" line="97"/>
        <source>Only the XML content must be copied: &lt;CrashReport&gt;...&lt;/CrashReport&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplication.cpp" line="98"/>
        <source>This is the system information to copy in the first field of this online form.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplication.cpp" line="106"/>
        <source>Generic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtCoreApplicationPrivate</name>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="589"/>
        <source>User interrupt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="590"/>
        <source>Is this process blocked? Answer &apos;Yes&apos; to generate a bug report.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="592"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="593"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="811"/>
        <source>Show help about options. SECTION may be empty or: all, wiki, latex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="815"/>
        <source>-help &lt;SECTION&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="816"/>
        <source>-version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="817"/>
        <source>Show version information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="818"/>
        <source>-app-version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="819"/>
        <source>Show short version information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="820"/>
        <source>-reportbug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="821"/>
        <source>Start bug report dialog, information about bug is passed through stdin. This option is used internally to report bugs if option -nobugreport is not specified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="824"/>
        <source>-reportint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="825"/>
        <source>Start bug report dialog, information about interruption is passed through stdin. This option is used internally to report interruptions if option -nobugreport is not specified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="828"/>
        <source>-nobugreport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="829"/>
        <source>Do not generate bug report in case of error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtbCoreApplicationPrivate</name>
    <message>
        <location filename="../src/QtCoreApplicationPrivate.cpp" line="852"/>
        <source>Option %1 requires an argument, see -help for details.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Random</name>
    <message>
        <location filename="../src/Random.cpp" line="173"/>
        <source>Repeating sequence on %1 samples after %2 generations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tar</name>
    <message>
        <location filename="../src/Tar.cpp" line="111"/>
        <source>Tar: Unsupported file mode access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tar.cpp" line="207"/>
        <source>File names greater than 99 charaters are currently not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tar.cpp" line="258"/>
        <source>Cannot read file in archive: bad header block, magic!=ustar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tar.cpp" line="262"/>
        <source>Cannot read file in archive: bad header block, typeflag!=0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tar.cpp" line="268"/>
        <source>Cannot read file in archive: bad header block, computed checksum (%1) != saved checksum (%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Time</name>
    <message>
        <location filename="../src/Time.cpp" line="83"/>
        <source>Error parsing time &apos;%1&apos; at position %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Version</name>
    <message>
        <location filename="../src/Version.cpp" line="79"/>
        <source>Error parsing version: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMLByteArrayStream</name>
    <message>
        <location filename="../src/XMLByteArrayStream.cpp" line="82"/>
        <source>Wrong tag, not a XMLClass byte array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLByteArrayStream.cpp" line="88"/>
        <source>Wrong version(%1), current version is %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMLClass</name>
    <message>
        <location filename="../src/XMLClass.cpp" line="437"/>
        <source>Cannot open file. Check file permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="440"/>
        <source>Cannot write to file. Check disk space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="443"/>
        <source>No doc type found at the beginning of the XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="446"/>
        <source>No version found at the beginning of the XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="449"/>
        <source>Unmatched ampersand and semi colon for XML special characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="452"/>
        <source>Unknwown XML special character</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="455"/>
        <source>Empty XML tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="458"/>
        <source>Unmatched XML tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="461"/>
        <source>Unmatched top level XML tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="464"/>
        <source>Empty context stack</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="467"/>
        <source>Error parsing tag content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="470"/>
        <source>Error parsing tag attributes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="473"/>
        <source>Error setting tag attributes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="476"/>
        <source>Error setting binary data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="479"/>
        <source>Wrong offset in binary file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="482"/>
        <source>Wrong binary tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="485"/>
        <source>Binary file not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="488"/>
        <source>Truncated tag at the end of XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="491"/>
        <source>Truncated string at the end of XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="494"/>
        <source>Truncated context at the end of XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="497"/>
        <source>Object not completely restored at the end of XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="500"/>
        <source>Object not found in XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="503"/>
        <source>%1 at line %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="504"/>
        <source>Parsing xml</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLClass.cpp" line="558"/>
        <source>read-only property : %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMLHeader</name>
    <message>
        <location filename="../src/XMLHeader.cpp" line="71"/>
        <source>Cannot write to file %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLHeader.cpp" line="76"/>
        <location filename="../src/XMLHeader.cpp" line="92"/>
        <source>Cannot open file %1 for writing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLHeader.cpp" line="153"/>
        <source>No contents.xml found in archive, trying old versions...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLHeader.cpp" line="227"/>
        <source>Wrong DOCTYPE at line </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLHeader.cpp" line="233"/>
        <source>Missing &apos;&gt;&apos; at line </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLHeader.cpp" line="273"/>
        <source>This XML has been generated with a newer release (%1).
  Your current release is %2. Please upgrade.
  ### WARNING ### : parsing XML at your own risks.
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMLParser</name>
    <message>
        <location filename="../src/XMLParser.cpp" line="189"/>
        <source>Unknown tag at line %1 for context %2: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLParser.cpp" line="210"/>
        <location filename="../src/XMLParser.cpp" line="713"/>
        <source>Looking for tag &apos;%1&apos;, found &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLParser.cpp" line="320"/>
        <source>Expecting tag %1, and found %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/XMLParser.cpp" line="613"/>
        <source>No value given for attribute %1 at line %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
