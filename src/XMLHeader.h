/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2009-06-08
**  Copyright: 2009-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef XMLHEADER_H
#define XMLHEADER_H

#include "XMLClass.h"
#include "Translations.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT XMLHeader : public XMLClass
{
  TRANSLATIONS("XMLHeader")
public:
  XMLHeader(XMLClass * object);
  XMLHeader(const XMLHeader& o);

  virtual const QString& xml_tagName() const;
  static const QString xmlHeaderTag;
  static const QString xmlHeaderOldTag;

  void addAlternateTag(const QString& t) {_alternateTags.append(t);}

  Error xml_saveFile(QString fileName, XMLContext * context=0, FileType ft=TarFile) const;
  QString xml_saveString(bool header=true, XMLContext * context=0) const;
  QByteArray xml_saveByteArray(XMLContext * context=0) const;
  Error xml_restoreFile(QString fileName, XMLContext * context=0, FileType ft=TarFile);
  Error xml_restoreString(QString str, XMLContext * context=0);
  Error xml_restoreByteArray(QByteArray data, XMLContext * context=0);
protected:
  virtual void xml_writeChildren(XML_WRITECHILDREN_ARGS) const;
  virtual void xml_attributes(XML_ATTRIBUTES_ARGS) const;
  virtual bool xml_setAttributes(XML_SETATTRIBUTES_ARGS);
  virtual XMLMember xml_member(XML_MEMBER_ARGS);
  virtual void xml_polishChild(XML_POLISHCHILD_ARGS);
  virtual bool xml_polished(XML_POLISHED_ARGS) {return _polished;}

  QString _tag;
  XMLClass * _object;
  bool _polished;
private:
  static void xml_saveDocType(XMLStream& s);
  static bool checkDocType(const QChar *& ptr, int& line);

  QStringList _alternateTags;
};

} // namespace QGpCoreTools

#endif // XMLHEADER_H
