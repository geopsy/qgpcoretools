/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-12-11
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef ARGUMENTSTDINREADER_H
#define ARGUMENTSTDINREADER_H

#include <QtCore>

#include "QGpCoreToolsDLLExport.h"
#include "Translations.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT ArgumentStdinReader
{
  TRANSLATIONS("ArgumentStdinReader")
public:
  virtual ~ArgumentStdinReader();

  bool read(int argc, char ** argv, bool ignoreStdin=false);
  bool read(QStringList fileNames);
  const QStringList& fileNames() const {return _fileNames;}
protected:
  virtual bool parse(QTextStream& s)=0;
private:
  QStringList _fileNames;
};

} // namespace QGpCoreTools

#endif // ARGUMENTSTDINREADER_H
