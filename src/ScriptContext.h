/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2011-08-11
**  Copyright: 2011-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#ifndef SCRIPTCONTEXT_H
#define SCRIPTCONTEXT_H

#include <QtCore>

#include "QGpCoreToolsDLLExport.h"

class QJSEngine;
class QScriptContext;
class QJSValue;

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT ScriptContext: public QObject
  {
    Q_OBJECT
  public:
    static void add(QJSEngine * engine);

    Q_INVOKABLE QString system(QString command);
    Q_INVOKABLE void print(QString text);
    Q_INVOKABLE QString rightJustified(QString text, int nChars, QString fillChar);
    Q_INVOKABLE QString leftJustified(QString text, int nChars, QString fillChar);
  };

} // namespace QGpCoreTools

#endif // SCRIPTCONTEXT_H
