/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2018-05-28
**  Copyright: 2018-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#include "ReadWriteLock.h"
#include "CoreApplicationPrivate.h"

namespace QGpCoreTools {

  /*!
    \class ReadWriteLock ReadWriteLock.h
    \brief Brief description of class still missing

    Full description of class still missing
  */

#ifdef LOG_MUTEX
  void ReadWriteLock::lockForRead(const char* file, int line)
  {
    if(App::verbosity()>=MUTEX_VERBOSITY) {
      QTime chrono;
      QReadWriteLock::lockForRead();
      int t=chrono.elapsed();
      if(t>1) {
        App::log(tr("[MUTEX] %1 ms in lockForRead() in %2:%3\n")
                      .arg(t).arg(file).arg(line));
      }
    } else {
      QReadWriteLock::lockForRead();
    }
  }

  void ReadWriteLock::lockForWrite(const char* file, int line)
  {
    if(App::verbosity()>=MUTEX_VERBOSITY) {
      QTime chrono;
      QReadWriteLock::lockForWrite();
      int t=chrono.elapsed();
      if(t>1) {
        App::log(tr("[MUTEX] %1 ms in lockForWrite() in %2:%3\n")
                       .arg(chrono.elapsed()).arg(file).arg(line));
      }
    } else {
      QReadWriteLock::lockForWrite();
    }
  }

  void ReadWriteLock::lockForRead()
  {
    if(App::verbosity()>=MUTEX_VERBOSITY) {
      QTime chrono;
      QReadWriteLock::lockForRead();
      int t=chrono.elapsed();
      if(t>1) {
        App::log(tr("[MUTEX] %1 ms in lockForRead()\n")
                       .arg(chrono.elapsed()));
      }
    } else {
      QReadWriteLock::lockForRead();
    }
  }

  void ReadWriteLock::lockForWrite()
  {
    if(App::verbosity()>=MUTEX_VERBOSITY) {
      QTime chrono;
      QReadWriteLock::lockForWrite();
      int t=chrono.elapsed();
      if(t>1) {
        App::log(tr("[MUTEX] %1 ms in lockForWrite()\n")
                       .arg(chrono.elapsed()));
      }
    } else {
      QReadWriteLock::lockForWrite();
    }
  }
#endif

} // namespace QGpCoreTools

