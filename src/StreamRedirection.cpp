/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-02-09
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "StreamRedirection.h"
#include "CoreApplication.h"
#include "Trace.h"

namespace QGpCoreTools {

  /*!
    \class StreamRedirection StreamRedirection.h
    \brief Begin and end stream redirection

  Begin and end stream redirection automatically when this object is deleted.
  This object is dedicated for short term redirections. Begin and end must be
  in the same thread.
*/

/*!
  \fn StreamRedirection::StreamRedirection(AbstractStream * s)
  Directs all output to stream \a s.
*/

/*!
  \fn StreamRedirection::~StreamRedirection()
  Ends redirection.
*/

} // namespace QGpCoreTools
