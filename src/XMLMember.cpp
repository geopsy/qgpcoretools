/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2009-06-07
**  Copyright: 2009-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "XMLMember.h"
#include "Trace.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

/*!
  \class XMLMember XMLMember.h
  \brief XML member description returned by XMLClass::xml_member()

  A member can be either referenced by an ID (any null or positive integer) or
  by a child pointer (of type XMLClass). Negative member IDs are reserved for
  flags such as permanent or temporary child (automatically deleted after
  XML_Class::xml_polish() call of its parent.

  A specific XMLContext can be passed for parsing the child or member which
  overrides the current context if any. Specific contexts are automatically
  deleted. Do not pass a reference to a local variable nor the context received
  in XMLClass::xml_member() arguments. The default context is always this last one.
*/

} // namespace QGpCoreTools
