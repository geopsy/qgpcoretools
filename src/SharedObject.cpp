/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2006-05-17
**  Copyright: 2006-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "SharedObject.h"

namespace QGpCoreTools {

/*!
  \class SharedObject SharedObject.h
  \brief A simple thread-safe shared object
*/

/*!
  \fn SharedObject::addReference()

  Call this function every time the object is used. The reference counter is increased by one.
*/

/*!
  \fn SharedObject::removeReference()

  Call this function every time the object is not used anymore and you do not want to delete it immediately.
  If the reference counter reaches 0, it returns true.
*/

/*!
  \fn SharedObject::removeReference(SharedObject *)

  Call this function every time the object is not used anymore and you want to delete it.
  If the reference counter reach 0, the object is automatically deleted.
*/

} // namespace QGpCoreTools
