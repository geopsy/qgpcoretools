/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2017-03-31
**  Copyright: 2017-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#include "UniqueID.h"
#include "Trace.h"
#include "Message.h"
#include "CoreApplication.h"

namespace QGpCoreTools {

  /*!
    \class UniqueID UniqueID.h
    \brief Brief description of class still missing

    Full description of class still missing
  */

  /*!
    Description of constructor still missing
  */
  UniqueID::UniqueID()
  {
    TRACE;
    _newId=0;
  }

  /*!
    Description of destructor still missing
  */
  UniqueID::~UniqueID()
  {
    TRACE;
  }

  /*!
    Returns a unique ID number.
  */
  int UniqueID::uniqueId(int resquestedId)
  {
    TRACE;
    if(resquestedId==-1) {
      return ++_newId;
    } else {
      _newId=0;
      if(countId(resquestedId, _newId)>0) {
        _newId++;
        App::log(Message::severityString(Message::Warning)
                 +tr(" ID %1 changed to %2\n").arg(resquestedId).arg(_newId));
        return _newId;
      } else {
        if(resquestedId>_newId) {
          _newId=resquestedId;
        }
        return resquestedId;
      }
    }
  }

} // namespace QGpCoreTools

