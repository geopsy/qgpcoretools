/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2009-08-31
**  Copyright: 2009-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef LEDS_H
#define LEDS_H

#include <QtCore>

#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT Leds : public QObject
{
  Q_OBJECT
public:
  Leds(QObject * parent=nullptr);
  ~Leds() {_self=0;}

  static void timer(int index, int msecOn, int msecOff);
  static void power(int index, bool on);
  static void flash(int index, int msec, bool on);
private slots:
  void execTimer0();
  void execTimer1();
  void execTimer2();
private:
  bool test(int index);
  void setTrigger(int index, const char * type);
  void powerInternal(int index, bool on);
  void setDelayOn(int index, int msec);
  void setDelayOff(int index, int msec);

  static Leds * _self;
  QTimer _timer[3];
  bool _defaultOn[3];
};

} // namespace QGpCoreTools

#endif // LEDS_H
