/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-05-14
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef ABSTRACTPARAMETERS_H
#define ABSTRACTPARAMETERS_H

#include <QtCore>

#include "QGpCoreToolsDLLExport.h"

#define PARAMETERS_VERSION 1
#define PARAMETERS_KEYWORDCOUNT_ARGS
#define PARAMETERS_TOTALKEYWORDCOUNT_ARGS
#define PARAMETERS_SETVERSION_ARGS int version
#define PARAMETERS_COLLECTKEYWORDS_ARGS KeywordMap& keywords, const QString& prefix, const QString& suffix
#define PARAMETERS_SETVALUE_ARGS int index, const QString& value, const QString& unit, const KeywordMap& keywords
#define PARAMETERS_TOSTRING_ARGS_DECL const QString& prefix=QString::null, const QString& suffix=QString::null
#define PARAMETERS_TOSTRING_ARGS_IMPL const QString& prefix, const QString& suffix

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT AbstractParameters
  {
  public:
    AbstractParameters();
    virtual ~AbstractParameters();

    void operator=(const AbstractParameters& o);
    virtual AbstractParameters * clone() const=0;

    int version() const {return _version;}

    struct Keyword {
      AbstractParameters * param;
      int index;
    };

    class QGPCORETOOLS_EXPORT KeywordMap: private QMap<QString, Keyword>
    {
    public:
      void add(const QString& key, AbstractParameters * param, int index);
      bool setValue(const QString& key, const QString& value, const QString& unit) const;
      QString key(const AbstractParameters * param, int index) const;
      int count() const {return QMap<QString, Keyword>::count();}
    };

    bool load(QString fileName);
    bool load(QTextStream& s, const QString& endPattern=QString::null);
    bool save(QString fileName) const;

    virtual QString toString(PARAMETERS_TOSTRING_ARGS_DECL) const;
    virtual void setVersion(PARAMETERS_SETVERSION_ARGS) {_version=version;}
    virtual void collectKeywords(PARAMETERS_COLLECTKEYWORDS_ARGS);
    virtual int totalKeywordCount(PARAMETERS_TOTALKEYWORDCOUNT_ARGS) const;
    bool parse(const QString& line, const KeywordMap * keywords=nullptr);
    bool setValues(const AbstractParameters& o);
  protected:
    virtual int keywordCount(PARAMETERS_KEYWORDCOUNT_ARGS) const;
    virtual bool setValue(PARAMETERS_SETVALUE_ARGS);
    void obsoleteKeyword(const KeywordMap& keywords, int index) const;
  private:
    KeywordMap * createKeywordMap();

    class ParameterValue
    {
    public:
      ParameterValue() {}
      inline ParameterValue(const QString& key, const QString& value, const QString& unit);
      inline ParameterValue(const ParameterValue& o);

      inline bool set(const KeywordMap& map) const;
    private:
      QString _key;
      QString _value;
      QString _unit;
    };

    int _version;
    QVector<ParameterValue> * _values;
  };

} // namespace QGpCoreTools

#endif // ABSTRACTPARAMETERS_H
