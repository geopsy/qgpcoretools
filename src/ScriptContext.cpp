/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2011-08-11
**  Copyright: 2011-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#include <QJSEngine>

#include "Trace.h"
#include "CoreApplication.h"
#include "ScriptContext.h"

namespace QGpCoreTools {

  /*!
    \class ScriptContext ScriptContext.h
    \brief Brief description of class still missing

    Full description of class still missing

    \todo class probably to be removed
  */

  /*!
    Description of destructor still missing
  */
  void ScriptContext::add(QJSEngine * engine)
  {
    QJSValue f;
    QJSValue extension=engine->newQObject(new ScriptContext());
    engine->globalObject().setProperty("print", extension.property("print"));
    engine->globalObject().setProperty("rightJustified", extension.property("rightJustified"));
    engine->globalObject().setProperty("leftJustified", extension.property("leftJustified"));
    engine->globalObject().setProperty("system", extension.property("system"));
  }

  QString ScriptContext::system(QString command)
  {
    TRACE;
    QProcess sysProcess;
    sysProcess.start(command);
    while(!sysProcess.waitForFinished()) {}
    return QString(sysProcess.readAll());
  }

  void ScriptContext::print(QString text)
  {
    TRACE;
    App::log(text+"\n");
  }

  QString ScriptContext::rightJustified(QString text, int nChars, QString fillChar)
  {
    TRACE;
    if(fillChar.isEmpty()) {
      fillChar=" ";
    }
    return text.rightJustified(nChars, fillChar[0]);
  }

  QString ScriptContext::leftJustified(QString text, int nChars, QString fillChar)
  {
    TRACE;
    if(fillChar.isEmpty()) {
      fillChar=" ";
    }
    return text.leftJustified(nChars, fillChar[0]);
  }

} // namespace QGpCoreTools
