/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2006-11-24
**  Copyright: 2006-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef TREEITEM_H
#define TREEITEM_H

#include "XMLClass.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class TreeContainer;

  class QGPCORETOOLS_EXPORT TreeItem: public XMLClass
  {
  public:
    TreeItem(TreeContainer * parent=nullptr);
    virtual ~TreeItem() {}

    virtual const QString& xml_tagName() const {return xmlTreeItemTag;}
    static const QString xmlTreeItemTag;

    TreeContainer * parent() {return _parent;}
    const TreeContainer * parent() const {return _parent;}

    bool isValidParent(const TreeContainer * parent) const;
    virtual void setParent(TreeContainer * parent);

    virtual bool isContainer() const {return false;}

    int rank() const;
    virtual void setRank(int r);
  private:
    TreeContainer * _parent;
  };

} // namespace QGpCoreTools

#endif // TREEITEM_H
