/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2016-08-26
**  Copyright: 2016-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#include "Trace.h"
#include "ColumnTextFile.h"
#include "ColumnTextParser.h"
#include "CoreApplication.h"
#include "XMLHeader.h"

namespace QGpCoreTools {

  /*!
    \class ColumnTextFile ColumnTextFile.h
    \brief Wrapper around ColumnTextParser to parse files

    Full description of class still missing
  */

  /*!
    Description of constructor still missing
  */
  ColumnTextFile::ColumnTextFile()
  {
    TRACE;
    _file=nullptr;
    _buffer=nullptr;
  }

  /*!
    Description of destructor still missing
  */
  ColumnTextFile::~ColumnTextFile()
  {
    TRACE;
    delete _file;
    delete _buffer;
  }

  /*!
    Sets file to parse.

    \sa setBuffer()
  */
  bool ColumnTextFile::setFile(QString fileName)
  {
    TRACE;
    delete _file;
    if(fileName.isEmpty()) {
      App::log(tr("Empty file name\n") );
      _file=nullptr;
      return false;
    } else {
      _file=new QFile(fileName);
      if( !_file->open(QIODevice::ReadOnly)) {
        App::log(tr("Cannot read file '%1'\n").arg(fileName) );
        delete _file;
        _file=nullptr;
        return false;
      }
      setText(new QTextStream(_file));
      startUpdates();
      return true;
    }
  }

  QString ColumnTextFile::fileName() const
  {
    TRACE;
    if(_file) {
      return _file->fileName();
    } else {
      return QString::null;
    }
  }

  /*!
    Sets buffer to parse.

    \sa setFile()
  */
  bool ColumnTextFile::setBuffer(const QString& str)
  {
    TRACE;
    delete _buffer;
    _buffer=new QString(str);
    setText(new QTextStream(_buffer));
    startUpdates();
    return true;
  }

  void ColumnTextFile::setParserFile(const QString& p)
  {
    TRACE;
    if(!p.isEmpty()) {
      stopUpdates();
      XMLHeader hdr(this);
      if(hdr.xml_restoreFile(p)==XMLClass::NoError) {
        if(_parserFile!=p) {
          _parserFile=p;
          emit parserFileChanged(_parserFile);
        }
      }
      emit typeChanged();
      startUpdates();
    }
  }

} // namespace QGpCoreTools

