/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2018-06-25
**  Copyright: 2018-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#ifdef MULTI_PRECISION

#include "ComplexMP.h"

namespace QGpCoreTools {

  /*!
    \class ComplexMP ComplexMP.h
    \brief Brief description of class still missing

    Full description of class still missing
  */

  const ComplexMP ComplexMP::null(64);

  ComplexMP& ComplexMP::operator*=(const ComplexMP& o)
  {
    RealMP re(_re);
    RealMP im(_im);

    _re*=o._re;
    im*=o._im;
    _re-=im;

    re*=o._im;
    _im*=o._re;
    _im+=re;

    return *this;
  }

  RealMP ComplexMP::abs2() const
  {
    RealMP re2(_re);
    re2*=_re;
    RealMP im2(_im);
    im2*=_im;
    re2+=im2;
    return re2;
  }

} // namespace QGpCoreTools

#endif // MUTLI_PRECISION
