/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-11-25
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef XMLGENERICITEM_H
#define XMLGENERICITEM_H

#include "TreeContainer.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT XMLGenericItem: public TreeContainer
  {
  public:
    XMLGenericItem(XMLGenericItem * parent=nullptr);
    XMLGenericItem(const QString& tag, XMLGenericItem * parent=nullptr);

    inline XMLGenericItem * childAt(int index);
    inline const XMLGenericItem * childAt(int index) const;
    inline XMLGenericItem * parent();
    inline const XMLGenericItem * parent() const;

    void setEnabled(bool e) {_enabled=e;}
    bool isEnabled() const {return _enabled;}
    bool isParentEnabled() const;

    const QVariant& value() const {return _value;}
    void setValue(const QVariant& v) {_value=v;}

    void setTag(const QString& tag) {_tag=tag;}
    virtual const QString& xml_tagName() const {return _tag;}
    static const QString xmlGenericTag;

    const XMLSaveAttributes& attributes() const {return _attributes;}
    XMLSaveAttributes& attributes() {return _attributes;}
    virtual void xml_attributes(XML_ATTRIBUTES_ARGS) const;
    virtual bool xml_setAttributes(XML_SETATTRIBUTES_ARGS);
    void addAttribute(const QString& key, const QString& value=QString::null) {_attributes.add(key, value);}

    virtual bool xml_inherits(const QString& tagName) const;
    QList<XMLGenericItem *> children(const QString& tag) const;
    void find(QList<XMLGenericItem *>& itemList, const XMLGenericItem * queryItem) const;
  protected:
    virtual TreeItem * createNewItem(TreeItem *) {return 0;}
    virtual XMLMember xml_member(XML_MEMBER_ARGS);
    virtual bool xml_setProperty(XML_SETPROPERTY_ARGS);
    virtual void xml_writeProperties(XML_WRITEPROPERTIES_ARGS) const;
    virtual void xml_writeChildren(XML_WRITECHILDREN_ARGS) const;
    virtual bool xml_setBinaryData(XML_SETBINARYDATA_ARGS);
  private:
    bool _enabled;
    QString _tag;
    XMLSaveAttributes _attributes;
    QVariant _value;
    QStringList _binDataFiles;
  };

  inline const XMLGenericItem * XMLGenericItem::childAt(int index) const
  {
    TRACE;
    return static_cast<const XMLGenericItem *>(TreeContainer::childAt(index));
  }

  inline XMLGenericItem * XMLGenericItem::childAt(int index)
  {
    TRACE;
    return static_cast<XMLGenericItem *>(TreeContainer::childAt(index));
  }

  inline XMLGenericItem * XMLGenericItem::parent()
  {
    TRACE;
    return static_cast<XMLGenericItem *>(TreeContainer::parent());
  }

  inline const XMLGenericItem * XMLGenericItem::parent() const
  {
    TRACE;
    return static_cast<const XMLGenericItem *>(TreeContainer::parent());
  }

} // namespace QGpCoreTools

#endif // XMLGENERICITEM_H
