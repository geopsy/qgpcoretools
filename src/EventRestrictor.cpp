/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2012-04-22
**  Copyright: 2012-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#include "EventRestrictor.h"

namespace QGpCoreTools {

  /*!
    \class EventRestrictor EventRestrictor.h
    \brief Restricts the number of events per unit of time
  */

  EventRestrictor::EventRestrictor(QObject * parent)
    : QObject(parent)
  {
    _chrono=0;
    setInterval(1000);
    _lastEvent.setSingleShot(true);
    connect(&_lastEvent, SIGNAL(timeout()), this, SLOT(timeout()));
  }

  EventRestrictor::~EventRestrictor()
  {
    if(_lastEvent.isActive()) {
      emit run();
    }
    delete _chrono;
  }

  void EventRestrictor::timeout()
  {
    if(!_chrono) {
      _chrono=new QTime;
    }
    emit run();
    _chrono->start();
  }

  void EventRestrictor::newEvent()
  {
    if(_chrono) {
      int delay=_chrono->elapsed();
      _lastEvent.stop();
      if(delay>=_lastEvent.interval()) {
        timeout();
      } else {
        _lastEvent.start();
      }
    } else {
      timeout();
    }
  }

} // namespace QGpCoreTools
