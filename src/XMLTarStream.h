/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-04-25
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef XMLTARSTREAM_H
#define XMLTARSTREAM_H

#include "XMLStream.h"
#include "Tar.h"

namespace QGpCoreTools {

class XMLTarStream : public XMLStream
{
public:
  XMLTarStream(QIODevice::OpenMode m);
  ~XMLTarStream();

  bool open(QString fileName);
  inline bool addXmlFile();

  virtual bool isMultiFile() const {return true;}
  inline virtual void addFile(QString fileName, const QByteArray& data);
  inline virtual bool file(QString fileName, QByteArray& data);
private:
  QIODevice::OpenMode _mode;
  QByteArray * _xml;
  Tar * _file;
};

inline void XMLTarStream::addFile(QString fileName, const QByteArray& data)
{
  TRACE;
  _file->addFile(fileName, data);
}

inline bool XMLTarStream::file(QString fileName, QByteArray& data)
{
  TRACE;
  return _file->file(fileName, data);
}

inline bool XMLTarStream::addXmlFile()
{
  TRACE;
  flush();
  return _file->addFile( "contents.xml", *_xml);
}

} // namespace QGpCoreTools

#endif // XMLTARSTREAM_H
