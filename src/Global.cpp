/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2012-11-21
**  Copyright: 2012-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#include "Global.h"

namespace QGpCoreTools {

  /*!
    \fn unique(QList<T>& list)

    Removes repeated entries. The container must be sorted.
  */

  /*!
    \fn unique(QList<T>& list, bool equal(const T& i1, const T& i2))

    This is an overloaded function.
    Uses the equal function instead of operator==() to compare the items.
  */

  /*!
    \fn unique(QVector<T>& v)

    Removes repeated entries. The container must be sorted.
  */

  /*!
    \fn unique(QVector<T>& v, bool equal(const T& i1, const T& i2))

    This is an overloaded function.
    Uses the equal function instead of operator==() to compare the items.
  */

  /*!
    Basic replacement of special characters for html
  */
  QString encodeToHtml(QString str)
  {
    str.replace("&","&amp;");
    str.replace("'","&apos;");
    str.replace("\"","&quot;");
    str.replace("<","&lt;");
    str.replace(">","&gt;");
    return str;
  }

  /*!
    We manage multi-threading, prevent OpenBlas to use several threads
  */
  void skipOpenBlasMultiThreading()
  {
    #ifdef HAS_OPEN_BLAS
    openblas_set_num_threads(1);
    #endif
  }

} // namespace QGpCoreTools

