/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-07-12
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef XMLMAP_H
#define XMLMAP_H

#include "XMLClass.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

template <class Key, class T>
class QGPCORETOOLS_EXPORT XMLMap : public XMLClass
{
public:
  XMLMap(QString tag, QMap<Key,T> * map);
  XMLMap(QString tag, const QMap<Key,T> * map);

  virtual const QString& xml_tagName() const {return _tag;}
  QMap<Key,T> * map() {return _map;}
protected:
  virtual void xml_writeProperties(XML_WRITEPROPERTIES_ARGS) const;
  virtual bool xml_setProperty(XML_SETPROPERTY_ARGS);
  virtual XMLMember xml_member(XML_MEMBER_ARGS);
private:
  QString _tag;
  QMap<Key,T> * _map;
};

template <class Key, class T>
XMLMap<Key,T>::XMLMap(QString tag, QMap<Key,T> * map)
{
  _tag=tag;
  _map=map;
}

template <class Key, class T>
XMLMap<Key,T>::XMLMap(QString tag, const QMap<Key,T> * map)
{
  _tag=tag;
  _map=const_cast<QMap<Key,T> *>(map);
}

template <class Key, class T>
void XMLMap<Key,T>::xml_writeProperties(XML_WRITEPROPERTIES_ARGS) const
{
  TRACE;
  Q_UNUSED(context);
  static const QString tag="value";
  static const QString key("key");
  XMLSaveAttributes att;
  QString& value=att.add(key);
  QMapIterator<Key,T> it(*_map);
  while(it.hasNext()) {
    it.next();
    value=it.key();
    writeProperty(s, tag, att, it.value());
  }
}

template <class Key, class T>
XMLMember XMLMap<Key,T>::xml_member(XML_MEMBER_ARGS)
{
  TRACE;
  Q_UNUSED(attributes);
  Q_UNUSED(context);
  if(tag=="value" ) return XMLMember(0);
  return XMLMember(XMLMember::Unknown);
}

template <class Key, class T>
bool XMLMap<Key,T>::xml_setProperty(XML_SETPROPERTY_ARGS)
{
  TRACE;
  Q_UNUSED(context);
  Q_UNUSED(tag);
  static const QString att="key";
  if(memberID==0) {
    XMLRestoreAttributeIterator it=attributes.find(att);
    if(it!=attributes.end()) {
      Key tmpKey;
      T tmpT;
      _map->insert(it.value().to(tmpKey), content.to(tmpT));
      return true;
    }
  }
  return false;
}

} // namespace QGpCoreTools

#endif // XMLMAP_H
