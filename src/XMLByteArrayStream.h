/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-04-25
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef XMLBYTEARRAYSTREAM_H
#define XMLBYTEARRAYSTREAM_H

#include "XMLStream.h"
#include "QGpCoreToolsDLLExport.h"
#include "Translations.h"
#include "Trace.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT XMLByteArrayStream : public XMLStream
{
  TRANSLATIONS("XMLByteArrayStream")
public:
  XMLByteArrayStream(QIODevice::OpenMode m);
  ~XMLByteArrayStream();

  bool open(QByteArray data);
  inline void addXmlFile();
  QByteArray data() const {return _data;}

  virtual bool isMultiFile() const {return true;}
  virtual void addFile(QString fileName, const QByteArray& data);
  virtual bool file(QString fileName, QByteArray& data);
private:
  QIODevice::OpenMode _mode;
  QByteArray * _xml;
  QByteArray _data;
  QMap<QString, QByteArray> _files;
  qint64 _lastPos;
};

inline void XMLByteArrayStream::addXmlFile()
{
  TRACE;
  flush();
  addFile( "contents.xml", *_xml);
}

} // namespace QGpCoreTools

#endif // XMLBYTEARRAYSTREAM_H
