/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-02-20
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "AbstractProgress.h"
#include "Global.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

/*!
  \class AbstractProgress AbstractProgress.h
  \brief Abstract progress monitor optimized for intensive computations

  Full description of class still missing
*/

/*!
  Description of constructor still missing
*/
AbstractProgress::AbstractProgress()
{
  TRACE;
  _value=0;
  _deltaValue=0;
  _maximum=-1;
  _lastTime=time(nullptr);
}

const QString& AbstractProgress::caption() const
{
  QMutexLocker ml(&_mutex);
  return _caption;
}

void AbstractProgress::setCaption(QString c)
{
  TRACE;
  QMutexLocker ml(&_mutex);
  _caption=c;
}

int AbstractProgress::maximum() const
{
  QMutexLocker ml(&_mutex);
  return _maximum;
}

void AbstractProgress::setMaximum(int val)
{
  TRACE;
  QMutexLocker ml(&_mutex);
  _deltaValue=0;
  _value=0;
  _maximum=val;
  _lastTime.fetchAndStoreOrdered(time(nullptr));
}

/*!
  Several increments may stack up if they arrive too fast, _deltaValue
  is the sum of all the intermediate increments.
*/
void AbstractProgress::increaseValueInternal()
{
  TRACE;
  _mutex.MUTEX_LOCK;
  _value+=_deltaValue;
  QString caption=_caption;
  int value=_value;
  int maximum=_maximum;
  _mutex.unlock();
  paint(caption, value, maximum); // May return to event loop , mutex should be unlocked
}

void AbstractProgress::setValueInternal(int val)
{
  TRACE;
  _mutex.MUTEX_LOCK;
  _value=val;
  QString caption=_caption;
  int value=_value;
  int maximum=_maximum;
  _mutex.unlock();
  paint(caption, value, maximum); // May return to event loop , mutex should be unlocked
}

QString AbstractProgress::valueString(int value, int maximum)
{
  if(value>maximum) {
    return QString::number(value);
  } else if(maximum>0){
    static const QString fmt("%1 %");
    return fmt.arg((100*value)/maximum);
  } else {
    return "100 %";
  }
}


} // namespace QGpCoreTools
