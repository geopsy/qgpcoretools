/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2010-03-21
**  Copyright: 2010-2019
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "CoreApplication.h"
#include "StringStream.h"
#include "XMLErrorReport.h"

namespace QGpCoreTools {

/*!
  \class XMLErrorReport XMLErrorReport.h
  \brief Brief description of class still missing

  Full description of class still missing
*/

  /*!
    Description of constructor still missing
  */
  XMLErrorReport::XMLErrorReport(Options o)
  {
    _options=o;
    StringStream * s=new StringStream(&_log);
    _redirection=new StreamRedirection(s);
  }

  /*!
    Description of constructor still missing
  */
  XMLErrorReport::~XMLErrorReport()
  {
    delete _redirection; // For security if exec() is not used.
  }

  /*!
    Entering this function, all the XML process is finished and stream can be deleted.
  */
  bool XMLErrorReport::exec(XMLClass::Error err)
  {
    delete _redirection;
    _redirection=0; // for a smooth destructor
    if(err!=XMLClass::NoError) {
      if(_options & NoMessageBox) {
        App::log(_title+"\n");
        App::setPrefix("  ");
        if(!_fileName.isEmpty()) {
          if(_options & Read) {
            App::log(tr("An error occurred while reading from file %1\n").arg(_fileName));
          } else if(_options & Write) {
            App::log(tr("An error occurred while writing to file %1\n").arg(_fileName));
          }
        }
        App::log(XMLClass::message(err)+"\n");
        if(!_log.isEmpty()) {
          App::log(_log+"\n");
        }
        App::setPrefix(QString::null);
        App::log("\n");
      } else {
        QString msg;
        if(_fileName.isEmpty()) {
          if(_options & Read) {
            msg=tr("An error occurred while reading XML content\n");
          } else if(_options & Write) {
            msg=tr("An error occurred while writing XML content\n");
          } else {
            msg=tr("An error occurred while accesing XML content\n");
          }
        } else {
          if(_options & Read) {
            msg=tr("An error occurred while reading from file %1\n").arg(_fileName);
          } else if(_options & Write) {
            msg=tr("An error occurred while writing to file %1\n").arg(_fileName);
          } else {
            msg=tr("An error occurred while accessing file %1\n").arg(_fileName);
          }
        }
        msg+=XMLClass::message(err)+"\n";
        if(!_log.isEmpty()) {
          msg+=tr("\nDetails:\n")+_log;
        }
        Message::critical(MSG_ID, _title, msg, true);
      }
      return false;
    } else {
      if(!_log.isEmpty()) {
        App::log(_title+"\n");
        App::setPrefix("  ");
        App::log(_log+"\n");
        App::setPrefix(QString::null);
        App::log("\n");
      }
      return true;
    }
  }

} // namespace QGpCoreTools
