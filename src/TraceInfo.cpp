/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-10-20
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "TraceInfo.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

/*!
  \class TraceInfo TraceInfo.h
  \brief Abstract class for all types of trace information

  Abstract class for all types of trace information. toString() returns a string
  that will be inserted in backtraces in case of error.
*/

QString TraceInfo::toString() const
{
  static const QString fmt("%1");
  QString val;
  switch(_val.type()) {
  case QVariant::ULongLong:
  case QVariant::UInt:
    val=fmt.arg(_val.toUInt(), 0, 16);
    break;
  case QVariant::Bool:
    val=fmt.arg(_val.toBool() ? "true" : "false");
    break;
  default:
    val=fmt.arg(_val.toString());
    break;
  }
  if(_valName) {
    return QString(_valName)+"="+val;
  } else {
    return val;
  }
}

} // namespace QGpCoreTools
