/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2006-07-06
**  Copyright: 2006-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "CacheItem.h"
#include "CacheProcess.h"

namespace QGpCoreTools {

/*!
  \class CacheItem CacheItem.h
  \brief Brief description of class still missing

  Full description of class still missing
*/

/*!
  \fn CacheItem::CacheItem()
  Description of constructor still missing
*/

/*!
  \fn CacheItem::CacheItem(const CacheItem& o)
  Description of copy constructor still missing
*/

/*!
  Assert if still locked or saved
*/
CacheItem::~CacheItem()
{
  ASSERT(_lockCount==0);
  ASSERT(!_saved);
  _adminLock.MUTEX_LOCK;
  for(QSet<CacheProcess *>::const_iterator it=_processes.begin(); it!=_processes.end(); it++) {
    (*it)->remove(this);
  }
  _adminLock.unlock();
}

/*!
  \fn CacheItem::lockData()
  lock() and unlock() are necessary to control allocation/deallocation of item data in cache. When
  an item is locked the cache manager cannot deallocate the item data. unlockData() leaves the item data in the cache
  and the data will be deallocated if requested by new allocations.

  Lock data for write.

  \sa unlockData(), constLockData()
*/

/*!
  \fn CacheItem::constLockData()
  lock() and unlock() are necessary to control allocation/deallocation of item data in cache. When
  an item is locked the cache manager cannot deallocate the item data. unlockData() leaves the item data in the cache
  and the data will be deallocated if requested by new allocations.

  Only lock data for read. Several threads can lock for read.

  \sa unlockData(), lockData()
*/

/*!
  \fn CacheItem::unlockData()

  \sa lockData(), constLockData()
*/

/*!
  Returns the predicted time of the next usage of this item.
*/
quint64 CacheItem::nextPredictedTime() const
{
  quint64 tMin=MAXIMUM_APPLICATION_LIFETIME;
  quint64 t;
  for(QSet<CacheProcess *>::iterator it=_processes.begin(); it!=_processes.end(); it++) {
    t=(*it)->predictedTime(this);
    if(t<tMin) {
      tMin=t;
    }
  }
  return tMin;
}

/*!
  \fn bool CacheItem::isAllocated()
  Implement this function to return true is data is allocated.
*/

/*!
  \fn int CacheItem::dataSize()
  Implement this function to return the size of the data to allocate in bytes.
*/

/*!
  \fn bool CacheItem::allocate()
  Implement this function to allocate the data. Return true if successful.
*/

/*!
  \fn void CacheItem::free()
  Implement this function to free the data. Never call this function directly, use Cache::free() instead.
*/

/*!
  \fn void CacheItem::save(QDir& d)
  Implement this function to save the data to disk when memory usage requires it.
*/

/*!
  \fn void CacheItem::load(QDir& d)
  Implement this function to load the data from disk upon request.
  \sa load()
*/

} // namespace QGpCoreTools
