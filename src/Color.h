/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2016-09-02
**  Copyright: 2016-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#ifndef COLOR_H
#define COLOR_H

#include <QtCore>

#include "Translations.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT Color
  {
    TRANSLATIONS("Color")
  public:
    Color() {_rgba=0;}
    Color(int r, int g, int b, int a=255) {setRgba(r, g, b, a);}
    Color(quint32 rgba) {_rgba=rgba;}
    Color(Qt::GlobalColor c);
    Color(const Color& o) {_rgba=o._rgba;}
    ~Color() {}

    bool operator==(const Color& o) const {return _rgba==o._rgba;}
    bool operator!=(const Color& o) const {return _rgba!=o._rgba;}

    quint32 rgba() const {return _rgba;}

    QString name() const;
    bool setNamedColor(const QString& n);

    int red() const {return (_rgba & 0x00FF0000) >> 16;}
    void setRed(int v) {_rgba&=0xFF00FFFF; _rgba|=v << 16;}

    int green() const {return (_rgba & 0x0000FF00) >> 8;}
    void setGreen(int v) {_rgba&=0xFFFF00FF; _rgba|=v << 8;}

    int blue() const {return _rgba & 0x000000FF;}
    void setBlue(int v) {_rgba&=0xFFFFFF00; _rgba|=v;}

    int alpha() const {return (_rgba & 0xFF000000) >> 24;}
    void setAlpha(int v) {_rgba&=0x00FFFFFF; _rgba|=v << 24;}

    int rgb() const {return _rgba & 0x00FFFFFF;}
    void setRgb(int v) {_rgba&=0xFF000000; _rgba|=v;}

    void hsv(int& h, int& s, int& v) const;

    inline void setRgba(int r, int g, int b, int a=255);
    void setHsva(int h, int s, int v, int a=255);

    void toGray();

    QString toString() const;
    void fromString(const QString& s);
  private:
    double gammaExpansion(double csrgb);
    double gammaCompression(double clinear);

    quint32 _rgba;
  };

  inline void Color::setRgba(int r, int g, int b, int a)
  {
    _rgba=(a << 24) |  (r << 16) |  (g << 8) |  b;
  }

} // namespace QGpCoreTools

#endif // COLOR_H

