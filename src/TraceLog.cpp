/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2010-05-24
**  Copyright: 2010-2019
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "TraceLog.h"
#include "Thread.h"
#include "CoreApplication.h"

namespace QGpCoreTools {

/*!
  \class TraceLog TraceLog.h
  \brief Brief description of class still missing

  Full description of class still missing
*/

QVector<TraceLog *> TraceLog::_mainVector;

/*!
  Description of constructor still missing
*/
TraceLog::TraceLog(const TraceStamp * s)
{
  QThread * t=QThread::currentThread();
  Thread * myt=qobject_cast<Thread *>(t);
  QVector<TraceLog *> * myLog;
  if(myt) {
    myLog=myt->logVector();
    myLog->append(this);
  } else if(t==CoreApplication::instance()->mainThread()) {
    myLog=&_mainVector;
    myLog->append(this);
  }
  _stamp=s;
}

QString TraceLog::backTrace(const QVector<TraceLog *> * log)
{
  QString list;
  for(int i=log->count()-1;i>=0;i--) {
    list += log->at(i)->toString();
  }
  return list;
}

QString TraceLog::toString() const
{
  QString str;
  str+=_stamp->toString() + "\n";
  for(QList<TraceInfo>::const_iterator it=_infos.begin();it!=_infos.end();it++) {
    str+="  "+it->toString() + "\n";
  }
  return str;
}

} // namespace QGpCoreTools
