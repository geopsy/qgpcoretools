/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-04-25
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef XMLSTREAM_H
#define XMLSTREAM_H

#include <QtCore>

namespace QGpCoreTools {

class XMLStream
{
protected:
  XMLStream(QByteArray * a, QIODevice::OpenMode m);
  XMLStream(QString * s, QIODevice::OpenMode m);
public:
  virtual ~XMLStream() {}

  void setFileName(QString fileName) {_fileName=fileName;}
  const QString& fileName() const {return _fileName;}

  const QString& indent() const {return _indent;}
  void incrementIndent() {_indent += "  ";}
  void decrementIndent() {_indent.chop(2);}

  virtual bool isMultiFile() const=0;
  virtual void addFile(QString fileName, const QByteArray& data) {Q_UNUSED(fileName); Q_UNUSED(data);}
  virtual bool file( ::QString fileName, QByteArray& data) {Q_UNUSED(fileName); Q_UNUSED(data); return false;}

  QTextStream& operator<<(const QString& string) {_xml << string; return _xml;}
  void flush() {_xml.flush();}
private:
  QString _fileName;

  QTextStream _xml;
  QString _indent;
};

} // namespace QGpCoreTools

#endif // XMLSTREAM_H
