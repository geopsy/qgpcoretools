/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-02-09
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef APPLICATIONHELP_H
#define APPLICATIONHELP_H

#include "QGpCoreToolsDLLExport.h"
#include "Translations.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT ApplicationHelp
{
  TRANSLATIONS("ApplicationHelp")
public:
  ApplicationHelp();
  ~ApplicationHelp();

  void exec(const char * group=nullptr);

  void addGroup(QString title, QByteArray section, int level=1);
  void addOption(QString option, QString comments);
  void setOptionSummary(QString optionSummary) {_optionSummary=optionSummary;}
  void setComments(QString comments) {_comments=comments;}
  void setSeeAlso(QString seeAlso) {_seeAlso=seeAlso;}
  void addExample(QString command, QString comments);

  QList<QByteArray> sections();
  static void print(QString p, QString linePrefix=QString::null, int indent=0);
  static QString getLine(QString& text, int maxLength, bool * newLine=nullptr);
  static QString encodeToHtml(QString str);
  static QString encodeToLatex(QString str);
private:
  struct Option {
    QString option;
    QString comments;
  };
  struct OptionGroup {
    QList<Option> options;
    QString title;
    QByteArray section;
    int level;
  };
  struct Example {
    QString command;
    QString comments;
  };
  QList<OptionGroup> _options;
  QList<Example> _examples;
  QString _optionSummary;
  QString _comments;
  QString _seeAlso;
  void print(const OptionGroup& g);
  void execHtml();
  void execLatex();
  static QString getCustomIndent(QString& text);
};

} // namespace QGpCoreTools

#endif // APPLICATIONHELP_H
