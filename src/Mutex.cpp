/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2018-05-28
**  Copyright: 2018-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#include "Mutex.h"
#include "CoreApplicationPrivate.h"

namespace QGpCoreTools {

  /*!
    \class Mutex Mutex.h
    \brief Brief description of class still missing

    Full description of class still missing
  */

#ifdef LOG_MUTEX
  void Mutex::lock(const char* file, int line)
  {
    if(App::verbosity()>=MUTEX_VERBOSITY) {
      QTime chrono;
      chrono.start();
      QMutex::lock();
      int t=chrono.elapsed();
      if(t>1) {
        App::log(tr("[MUTEX] %1 ms in lock() in %2:%3\n")
                       .arg(t).arg(file).arg(line));
      }
    } else {
      QMutex::lock();
    }
  }

  void Mutex::lock()
  {
    if(App::verbosity()>=MUTEX_VERBOSITY) {
      QTime chrono;
      chrono.start();
      QMutex::lock();
      int t=chrono.elapsed();
      if(t>1) {
        App::log(tr("[MUTEX] %1 ms in lock()\n")
                       .arg(t));
      }
    } else {
      QMutex::lock();
    }
  }
#endif

} // namespace QGpCoreTools

