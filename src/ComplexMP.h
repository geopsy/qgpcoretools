/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2018-06-25
**  Copyright: 2018-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#ifndef COMPLEXMP_H
#define COMPLEXMP_H

#ifdef MULTI_PRECISION

#include "RealMP.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT ComplexMP
  {
  public:
    ComplexMP(const ComplexMP& c) : _re(c._re), _im(c._im) {}
    ComplexMP(const RealMP& re, const RealMP& im) : _re(re), _im(im) {}
    ComplexMP(mpfr_prec_t p, double re=0, double im=0) : _re(p, re), _im(p, im) {}
    ~ComplexMP() {}

    bool operator==(const ComplexMP& o) const {return _re==o._re && _im==o._im;}
    bool operator!=(const ComplexMP& o) const {return _re!=o._re && _im!=o._im;}

    ComplexMP& operator=(const ComplexMP& o) {_re=o._re; _im=o._im; return *this;}
    ComplexMP& operator+=(const ComplexMP& o) {_re+=o._re; _im+=o._im; return *this;}
    ComplexMP& operator-=(const ComplexMP& o) {_re-=o._re; _im-=o._im; return *this;}
    ComplexMP& operator*=(const ComplexMP& o);
    inline ComplexMP& operator/=(const ComplexMP& o);

    ComplexMP& operator=(const RealMP& r) {_re=r; _im=0.0; return *this;}
    ComplexMP& operator+=(const RealMP& r) {_re+=r; return *this;}
    ComplexMP& operator-=(const RealMP& r) {_re-=r; return *this;}
    ComplexMP& operator*=(const RealMP& r) {_re*=r; _im*=r; return *this;}
    inline ComplexMP& operator/=(const RealMP& r);

    ComplexMP& operator=(const double& r) {_re=r; _im=0.0; return *this;}
    ComplexMP& operator+=(const double& r) {_re+=r; return *this;}
    ComplexMP& operator-=(const double& r) {_re-=r; return *this;}
    ComplexMP& operator*=(const double& r) {_re*=r; _im*=r; return *this;}
    ComplexMP& operator/=(const double& r) {return operator/=(RealMP(r));}

    RealMP abs() const;
    RealMP abs2() const;
    void conjugate() {_im.negate();}

    mpfr_prec_t precision() const {return _re.precision();}

    static const ComplexMP null;
  private:
    RealMP _re, _im;
  };

  inline ComplexMP operator+(const double& d, const ComplexMP& c);
  inline ComplexMP operator-(const double& d, const ComplexMP& c);
  inline ComplexMP operator*(const double& d, const ComplexMP& c);
  inline ComplexMP operator/(const double& d, const ComplexMP& c);

  inline ComplexMP sqrt(const ComplexMP& c);
  inline ComplexMP cos(const ComplexMP& c);
  inline ComplexMP sin(const ComplexMP& c);
  inline ComplexMP tan(const ComplexMP& c);
  inline ComplexMP asin(const ComplexMP& c);
  inline ComplexMP log(const ComplexMP& c);
  inline ComplexMP exp(const ComplexMP& c);
  inline ComplexMP conjugate(const ComplexMP& c);
  inline ComplexMP inverse(const ComplexMP& c);
  inline RealMP abs(const ComplexMP& c);
  inline RealMP abs2(const ComplexMP& c);

  inline ComplexMP& ComplexMP::operator/=(const ComplexMP& o)
  {
    operator*=(QGpCoreTools::conjugate(o));
    operator/=(o.abs2());
    return *this;
  }

  inline ComplexMP& ComplexMP::operator/=(const RealMP& o)
  {
    RealMP inv(1.0);
    inv/=o;
    _re*=inv;
    _im*=inv;
    return *this;
  }

  inline ComplexMP conjugate(const ComplexMP& o)
  {
    ComplexMP c(o);
    c.conjugate();
    return c;
  }

  inline RealMP ComplexMP::abs() const
  {
    return sqrt(abs2());
  }

} // namespace QGpCoreTools

#endif // MULTI_PRECISION
#endif // COMPLEXMP_H

