/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2006-07-06
**  Copyright: 2006-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef CACHEITEM_H
#define CACHEITEM_H

#include <time.h>

#include "Global.h"
#include "ApplicationClock.h"
#include "QGpCoreToolsDLLExport.h"
#include "Trace.h"
#include "Mutex.h"
#include "ReadWriteLock.h"

namespace QGpCoreTools {

  class CacheProcess;

  class QGPCORETOOLS_EXPORT CacheItem
  {
  public:
    inline CacheItem( );
    inline CacheItem(const CacheItem& o);
    virtual ~CacheItem();

    inline void addProcess(CacheProcess * p) const;
    inline void removeProcess(CacheProcess * p) const;
    quint64 nextPredictedTime() const;
  protected:
    friend class Cache;
    friend class AllocatedCacheItem;

    inline void lockData() const;
    inline void constLockData() const;
    inline void unlockData() const;

    inline void lockAdmin() const {_adminLock.MUTEX_LOCK;}
    inline void unlockAdmin() const {_adminLock.unlock();}

    // Memory management for cached data
    bool isSaved() const {return _saved;}
    virtual bool isAllocated() const=0;
    virtual qint64 dataSize() const=0;
    virtual bool allocate() const=0;
    virtual void free() const=0;
    virtual void save(QDir& d) const=0;
    virtual void load(QDir& d) const=0;

    inline QString swapFileName() const {return QString("cache_data_%1").arg(reinterpret_cast<qint64>(this), 0, 16);}
    virtual QString debugName() const {return swapFileName();}
  private:
    mutable Mutex _adminLock;
    mutable ReadWriteLock _dataLock;
    mutable QAtomicInt _lockCount;
    mutable QSet<CacheProcess *> _processes;
    mutable quint64 _lastAccess;
    mutable bool _saved:1;
  };

  inline CacheItem::CacheItem() : _dataLock(ReadWriteLock::Recursive)
  {
    TRACE;
    _lastAccess=ApplicationClock::elapsed();
    _saved=false;
    _lockCount=0;
  }

  inline CacheItem::CacheItem(const CacheItem& /*o*/) : _dataLock(ReadWriteLock::Recursive)
  {
    TRACE;
    _lastAccess=ApplicationClock::elapsed();
    _saved=false;
    _lockCount=0;
  }

  inline void CacheItem::lockData() const
  {
    _dataLock.lockForWrite();
    _lockCount++;
  }

  inline void CacheItem::constLockData() const
  {
    _dataLock.lockForRead();
    _lockCount++;
  }

  inline void CacheItem::unlockData() const
  {
    _lockCount--;
    _lastAccess=ApplicationClock::elapsed();
    _dataLock.unlock();
  }

  inline void CacheItem::addProcess(CacheProcess * p) const
  {
    _adminLock.MUTEX_LOCK;
    _processes.insert(p);
    _adminLock.unlock();
  }

  inline void CacheItem::removeProcess(CacheProcess * p) const
  {
    _adminLock.MUTEX_LOCK;
    _processes.remove(p);
    _adminLock.unlock();
  }

} // namespace QGpCoreTools

#endif // CACHEITEM_H
