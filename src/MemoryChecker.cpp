/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-01-29
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "CoreApplication.h"
#include "MemoryChecker.h"
#include "MemoryCheckerPrivate.h"

namespace QGpCoreTools {

/*!
  \class MemoryChecker MemoryChecker.h
  \brief Brief description of class still missing

  This is just a redefinition of the basic allocation operators
  put #include "MemoryChecker.h" at the beginning of all your files.
  It must be the last include to avoid conflicts with other libraries.

  In order to use memory checker, you must init CoreApplication:
  \code
int main(int argc, char ** argv)
{
  CoreApplication a(argc, argv);
  ...
  return 0
}
  \endcode
*/

} // namespace QGpCoreTools
