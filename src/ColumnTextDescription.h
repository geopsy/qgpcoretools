/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-11-01
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef COLUMNTEXTDESCRIPTION_H
#define COLUMNTEXTDESCRIPTION_H

#include "QGpCoreToolsDLLExport.h"
#include "XMLClass.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT ColumnTextDescription : public XMLClass
{
public:
  ColumnTextDescription() {
    _type=0;
    _factor=1.0;
  }
  ColumnTextDescription(const ColumnTextDescription& o) : XMLClass() {
    _type=o._type;
    _factor=o._factor;
    _replaceRx=o._replaceRx;
    _replaceAfter=o._replaceAfter;
  }

  virtual const QString& xml_tagName() const {return xmlColumnTextDescriptionTag;}
  static const QString xmlColumnTextDescriptionTag;

  int type() const {return _type;}
  void setType(int t) {_type=t;}

  double factor() const {return _factor;}
  void setFactor(double f) {_factor=f;}

  QString replaceRx() const {return _replaceRx;}
  void setReplaceRx(QString r) {_replaceRx=r;}

  QString replaceAfter() const {return _replaceAfter;}
  void setReplaceAfter(QString r) {_replaceAfter=r;}
protected:
  virtual void xml_writeProperties(XML_WRITEPROPERTIES_ARGS) const;
  virtual bool xml_setProperty(XML_SETPROPERTY_ARGS);
  virtual XMLMember xml_member(XML_MEMBER_ARGS);
private:
  int _type;
  double _factor;
  QString _replaceRx, _replaceAfter;
};

} // namespace QGpCoreTools

#endif // COLUMNTEXTDESCRIPTION_H
