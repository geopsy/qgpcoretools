/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2010-03-21
**  Copyright: 2010-2019
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef XMLERRORREPORT_H
#define XMLERRORREPORT_H

#include "StringStream.h"
#include "StreamRedirection.h"
#include "XMLClass.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT XMLErrorReport
  {
  public:
    enum Option {Read=0x01, Write=0x2, NoMessageBox=0x04};
    Q_DECLARE_FLAGS(Options, Option)

    XMLErrorReport(Options o);
    ~XMLErrorReport();

    void setTitle(const QString& t) {_title=t;}
    void setFileName(const QString& f) {_fileName=f;}

    bool exec(XMLClass::Error err);
  private:
    Options _options;
    QString _log, _fileName, _title;
    StreamRedirection * _redirection;
  };

  Q_DECLARE_OPERATORS_FOR_FLAGS(QGpCoreTools::XMLErrorReport::Options)

} // namespace QGpCoreTools

#endif // XMLERRORREPORT_H
