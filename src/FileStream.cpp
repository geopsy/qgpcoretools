/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2010-09-11
**  Copyright: 2010-2019
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "FileStream.h"

namespace QGpCoreTools {

  /*!
    \class FileStream FileStream.h
    \brief Brief description of class still missing

    Full description of class still missing
  */

  /*!
    Description of constructor still missing
  */
  FileStream::FileStream(const QString& fileName)
      : AbstractStream(), _f(fileName)
  {
    if(_f.open(QIODevice::WriteOnly)) {
      _s.setDevice(&_f);
      _s.setCodec("UTF-16");
      _s.setGenerateByteOrderMark(true);
    } else { // File cannot be open for writing, redirect to stdout
      _f.open(stdout, QIODevice::WriteOnly);
      _s.setDevice(&_f);
      send(0, tr("warning: cannot open file %1\n").arg(fileName));
    }
  }

} // namespace QGpCoreTools
