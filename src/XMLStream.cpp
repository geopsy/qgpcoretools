/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-04-25
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/


#include "XMLStream.h"
#include "Trace.h"

namespace QGpCoreTools {

/*!
  \class XMLStream XMLstream.h
  \brief Brief description of class still missing

  Full description of class still missing
*/

/*!
  Save or restore from/to a byte array (both modes accepted)
*/
XMLStream::XMLStream(QByteArray * a, QIODevice::OpenMode m)
  :  _xml(a, m)
{
  TRACE;
  if(m==QIODevice::WriteOnly) {
    _xml.setRealNumberNotation(QTextStream::SmartNotation);
    _xml.setRealNumberPrecision(16);
  }
  _xml.setCodec("UTF-16");
  _xml.setGenerateByteOrderMark (true);
}

/*!
  Save or restore from/to a string (both modes accepted)
*/
XMLStream::XMLStream(QString * s, QIODevice::OpenMode m)
  :  _xml(s, m)
{
  TRACE;
  if(m==QIODevice::WriteOnly) {
    _xml.setRealNumberNotation(QTextStream::SmartNotation);
    _xml.setRealNumberPrecision(16);
  }
  _xml.setCodec("UTF-16");
  _xml.setGenerateByteOrderMark (true);
}

} // namespace QGpCoreTools
