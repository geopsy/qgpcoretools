/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2009-07-22
**  Copyright: 2009-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef DYNAMICBUFFER_H
#define DYNAMICBUFFER_H

#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT DynamicBuffer
{
public:
  DynamicBuffer();
  ~DynamicBuffer();

  void add(const char * data, int byteCount);

  int availableBytes() const {return _bufferIn-_bufferOut;}
  const char * data() {return _buffer+_bufferOut;}
  void release(int byteCount);
  void clear();
private:
  int _bufferCapacity;
  int _bufferIn;
  int _bufferOut;
  char * _buffer;
};

} // namespace QGpCoreTools

#endif // DYNAMICBUFFER_H
