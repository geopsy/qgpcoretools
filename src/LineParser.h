/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2009-03-15
**  Copyright: 2009-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef LINEPARSER_H
#define LINEPARSER_H

#include "QGpCoreToolsDLLExport.h"
#include "StringSection.h"
#include "DateTime.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT LineParser
{
public:
  LineParser();
  LineParser(const QString& str);
  LineParser(const StringSection& str);
  ~LineParser() {}

  void setString(const QString& str);
  void setString(const StringSection& str);
  QString string() const {return _strSection.toString();}

  void setDelimiters(const QString& d) {_delimiters=d;}
  const QString& delimiters() const {return _delimiters;}

  void setSkipEmpty(bool s) {_skipEmpty=s;}
  bool skipEmpty() const {return _skipEmpty;}

  void setQuotes(bool q) {_quotes=q;}
  bool quotes() const {return _quotes;}

  int count();
  inline const StringSection& value(int index);
  inline QString toString(int index, bool& ok);
  QString toString(int indexBegin, int indexEnd, bool& ok);
  inline bool toBool(int index, bool& ok);
  inline int toInt(int index, bool& ok, int base=10);
  inline uint toUInt(int index, bool& ok, int base=10);
  inline qlonglong toLongLong(int index, bool& ok, int base=10);
  inline double toDouble(int index, bool& ok);
  inline DateTime toTime(int index, const QString& format, bool& ok);
  int startsAt(int index, bool& ok);
  int endsAt(int index, bool& ok);
private:
  const StringSection& parse(int index);

  QVector<StringSection> _values;
  const QChar * _ptr;
  StringSection _strSection;
  QString _str;
  QString _delimiters;
  bool _skipEmpty, _quotes;
};

inline const StringSection& LineParser::value(int index)
{
  if(index<_values.count()) return _values.at(index);
  return parse(index);
}

inline QString LineParser::toString(int index, bool& ok)
{
  const StringSection& v=value(index);
  if(v.isValid()) {
    return v.toString();
  } else {
    ok=false;
    return QString::null;
  }
}

inline bool LineParser::toBool(int index, bool& ok)
{
  const StringSection& v=value(index);
  if(v.isValid()) {
    return v.toBool();
  } else {
    ok=false;
    return true;
  }
}

inline int LineParser::toInt(int index, bool& ok, int base)
{
  if(ok) {
    return value(index).toInt(&ok, base);
  } else {
    return 0;
  }
}

inline uint LineParser::toUInt(int index, bool& ok, int base)
{
  if(ok) {
    return value(index).toUInt(&ok, base);
  } else {
    return 0;
  }
}

inline qlonglong LineParser::toLongLong(int index, bool& ok, int base)
{
  if(ok) {
    return value(index).toLongLong(&ok, base);
  } else {
    return 0;
  }
}

inline double LineParser::toDouble(int index, bool& ok)
{
  if(ok) {
    return value(index).toDouble(&ok);
  } else {
    return 0.0;
  }
}

inline DateTime LineParser::toTime(int index, const QString& format, bool& ok)
{
  if(ok) {
    return value(index).toTime(format, &ok);
  } else {
    return DateTime::null;
  }
}

} // namespace QGpCoreTools

#endif // LINEPARSER_H
