#ifndef QGPCORETOOLSDLLEXPORT_H
#define QGPCORETOOLSDLLEXPORT_H

/* *_STATIC and MAKE_*_DLL are defined by the capitalization of
  the package name, included in the compiler options (only for Windows).
  Use directly WIN32 to allow transparent usage with or without Qt.
  QGpCoreToolsStatic.h may contain QGPCORETOOLS_STATIC macro definition.
  This define was introduced there to mark this library as static for
  all projects that link to this library.
*/

#include "QGpCoreToolsStatic.h"

#if defined(WIN32) && !defined(QGPCORETOOLS_STATIC)
#ifdef MAKE_QGPCORETOOLS_DLL
# define QGPCORETOOLS_EXPORT __declspec(dllexport)
#else
# define QGPCORETOOLS_EXPORT __declspec(dllimport)
#endif
#else
# define QGPCORETOOLS_EXPORT
#endif

#endif  // QGPCORETOOLSDLLEXPORT_H

