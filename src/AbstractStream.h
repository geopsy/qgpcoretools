/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2010-09-11
**  Copyright: 2010-2019
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef ABSTRACTSTREAM_H
#define ABSTRACTSTREAM_H

#include <QtCore>

#include "SharedObject.h"
#include "QGpCoreToolsDLLExport.h"
#include "Mutex.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT AbstractStream: public SharedObject
  {
  public:
    AbstractStream();
    virtual ~AbstractStream();

    // rtti not available under Windows, let identification of stream type
    enum ClassId {StandardId=0, StringId, FileId, LogViewId, CustomId};
    virtual ClassId classId() const=0;

    void setPrefix(const QString & prefix);
    QString prefix() const;

    void setActive(bool a);
    bool isActive() const;

    static int applicationVerbosity() {return _applicationVerbosity;}
    static void setApplicationVerbosity(int verbosity);

    void setNext(AbstractStream * s) {_next=s;}
    AbstractStream * next() const {return _next;}

    void setPrevious(AbstractStream * s);
    AbstractStream * previous() const {return _previous;}

    void send(int verbosity, const QString& val);
  protected:
    virtual void sendToStream(const QString& val)=0;
    virtual void flushStream()=0;

    void lock() {_mutex.lock();}
    void unlock() {_mutex.unlock();}
  private:
    inline QString timeStampedPrefix();

    AbstractStream * _next, * _previous;
    static int _applicationVerbosity;
    bool _active;
    bool _lineOpen;
    enum PrefixType {NoPrefix, String, TimeStamp};
    PrefixType _prefixType;
    QString _prefix1, _prefix2;
    // Not a Mutex to avoid recursion
    mutable QMutex _mutex;
  };

} // namespace QGpCoreTools

#endif // ABSTRACTSTREAM_H
