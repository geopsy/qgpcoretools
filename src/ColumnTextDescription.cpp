/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-11-01
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "ColumnTextDescription.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

/*!
  \class ColumnTextDescription ColumnTextDescription.h
  \brief Storage for the decription of column in a ColumnTextParser

  Full description of class still missing
*/

const QString ColumnTextDescription::xmlColumnTextDescriptionTag="ColumnTextDescription";

void ColumnTextDescription::xml_writeProperties(XML_WRITEPROPERTIES_ARGS) const
{
  TRACE;
  Q_UNUSED(context);
  writeProperty(s, "type", _type);
  writeProperty(s, "factor", _factor);
  writeProperty(s, "replaceRx", _replaceRx);
  writeProperty(s, "replaceAfter", _replaceAfter);
}

XMLMember ColumnTextDescription::xml_member(XML_MEMBER_ARGS)
{
  TRACE;
  Q_UNUSED(attributes);
  Q_UNUSED(context);
  if(tag=="type") return XMLMember(0);
  else if(tag=="factor") return XMLMember(1);
  else if(tag=="replaceRx") return XMLMember(2);
  else if(tag=="replaceAfter") return XMLMember(3);
  return XMLMember(XMLMember::Unknown);
}

bool ColumnTextDescription::xml_setProperty(XML_SETPROPERTY_ARGS)
{
  TRACE;
  Q_UNUSED(tag);
  Q_UNUSED(attributes);
  Q_UNUSED(context);
  switch (memberID) {
  case 0: _type=content.toInt(); return true;
  case 1: _factor=content.toDouble(); return true;
  case 2: _replaceRx=content.toString(); return true;
  case 3: _replaceAfter=content.toString(); return true;
  default: return false;
  }
}

} // namespace QGpCoreTools
