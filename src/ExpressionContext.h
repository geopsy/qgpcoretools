/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-04-20
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef EXPRESSIONCONTEXT_H
#define EXPRESSIONCONTEXT_H

#include <QtCore>

#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

class ExpressionStorage;
class ExpressionAction;

class QGPCORETOOLS_EXPORT ExpressionContext
{
public:
  ExpressionContext(ExpressionContext * parent=0) {_parent=parent;}
  virtual ~ExpressionContext();

  void addVariable(const QString& name, const QVariant& val) {addVariable(name, QString::null, val);}
  void addVariable(const QString& name, const QString& index, const QVariant& val);
  int addVariable(const QString& name, ExpressionStorage * var);

  QStringList variables() const;
  bool isValidVariable(const QString& name) const;
  QVariant variableValue(const QString& name, const QString& index=QString::null) const;
  ExpressionStorage * variable(int varIndex) const {return _varValues[varIndex];}

  virtual QStringList functions() const;
  virtual bool isValidFunction(const QString& name) const;
  virtual QVariant functionValue(const QString& name, const QVector<ExpressionAction *>& args) const;
private:
  ExpressionContext * _parent;
  QMap<QString, int> _varNames;
  QVector<ExpressionStorage *> _varValues;
};

} // namespace QGpCoreTools

#endif // EXPRESSIONCONTEXT_H
