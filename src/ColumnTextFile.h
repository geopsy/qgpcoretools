/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2016-08-26
**  Copyright: 2016-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#ifndef COLUMNTEXTFILE_H
#define COLUMNTEXTFILE_H

#include "ColumnTextParser.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT ColumnTextFile : public ColumnTextParser
  {
    Q_OBJECT
  public:
    ColumnTextFile();
    virtual ~ColumnTextFile();

    virtual bool setFile(QString fileName);
    bool setBuffer(const QString& str);

    void setParserFile(const QString& p);
    const QString& parserFile() const {return _parserFile;}

    QString fileName() const;
  signals:
    void parserFileChanged(const QString& parserFile);
    void typeChanged();
  private:
    QFile * _file;
    QString * _buffer;
    QString _parserFile;
  };

} // namespace QGpCoreTools

#endif // COLUMNTEXTFILE_H

