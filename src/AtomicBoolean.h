/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2011-01-11
**  Copyright: 2011-2019
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef ATOMICBOOLEAN_H
#define ATOMICBOOLEAN_H

#include <QtCore>

#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT AtomicBoolean
  {
  public:
    AtomicBoolean(bool v=true) {_value=v;}
    AtomicBoolean(const AtomicBoolean& o) {_value=o._value;}

    void operator=(const AtomicBoolean& o) {_value=o._value;}
    void operator=(bool o) {_value=o;}

    inline bool operator==(const AtomicBoolean& o);
    inline bool operator==(bool o);

    void setValue(bool v) {_value.testAndSetOrdered(!v, v);}
    bool value() const {return _value.testAndSetOrdered(true, true);}
  private:
    mutable QAtomicInt _value;
  };

  inline bool AtomicBoolean::operator==(const AtomicBoolean& o)
  {
#if(QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
    return _value.load()==o._value.load();
#else
    return _value==o._value;
#endif
  }

  inline bool AtomicBoolean::operator==(bool o)
  {
#if(QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
    return _value.load()==o;
#else
    return _value==o;
#endif
  }

} // namespace QGpCoreTools

#endif // ATOMICBOOLEAN_H
