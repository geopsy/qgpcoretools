/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-10-20
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "TraceBug.h"
#include "Thread.h"
#include "CoreApplication.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

/*!
  \class TraceBug TraceBug.h
  \brief Brief description of class still missing

  Full description of class still missing
*/

QStack<TraceBug *> TraceBug::_mainStack;

/*!
  Description of constructor still missing
*/
TraceBug::TraceBug(const TraceStamp * s)
{
  QThread * t=QThread::currentThread();
  Thread * myt=qobject_cast<Thread *>(t);
  if(myt) {
    _myStack=myt->bugStack();
    _myStack->push(this);
  } else if(t==CoreApplication::instance()->mainThread()) {
    _myStack=&_mainStack;
    _myStack->push(this);
  } else {
    _myStack=0;
  }
  _stamp=s;
}

/*!
  Description of destructor still missing
*/
TraceBug::~TraceBug()
{
  if(_myStack) {
    _myStack->pop();
  }
}

QString TraceBug::backTrace(const QStack<TraceBug *> * stack)
{
  QString list;
  for(int i=stack->count()-1;i>=0;i--) {
    list += stack->at(i)->toString();
  }
  return list;
}

QString TraceBug::toString() const
{
  QString str;
  str+=_stamp->toString() + "\n";
  for(QList<TraceInfo>::const_iterator it=_infos.begin();it!=_infos.end();it++) {
    str+="  "+it->toString() + "\n";
  }
  return str;
}

} // namespace QGpCoreTools
