/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-10-20
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef TRACEBUG_H
#define TRACEBUG_H

#include "QGpCoreToolsDLLExport.h"
#include "TraceInfo.h"

namespace QGpCoreTools {

#define TRACE_BUG \
  static TraceStamp _traceBugStamp_(__PRETTY_FUNCTION__, __LINE__); \
  TraceBug _traceBug_0(&_traceBugStamp_); \
  TraceBug * _traceBug_=&_traceBug_0; \
  Q_UNUSED(_traceBug_);

// Better to use ##__COUNTER__ macro but not working with gcc 4.1.3, maybe gcc 4.3
#define TRACE_BUG_N(index) \
  static TraceStamp _traceBugStamp_##index(__PRETTY_FUNCTION__, __LINE__); \
  TraceBug _traceBug_##index(&_traceBugStamp_##index); \
  _traceBug_=&_traceBug_##index;

#define TRACE_BUG_POINTER(val) \
  _traceBug_->append( # val, val);

#define TRACE_BUG_INT(val) \
  _traceBug_->append( # val, val);

#define TRACE_BUG_BOOL(val) \
  _traceBug_->append( # val, val);

#define TRACE_BUG_DOUBLE(val) \
  _traceBug_->append( # val, val);

#define TRACE_BUG_STRING(val) \
  _traceBug_->append( # val, val);

#define TRACE_BUG_PRINT(val) \
  _traceBug_->append(0, val);

class TraceStamp;

class QGPCORETOOLS_EXPORT TraceBug
{
public:
  TraceBug(const TraceStamp * stamp);
  ~TraceBug();

  inline void append(const char * valName, const void * val);
  inline void append(const char * valName, QVariant val);

  static QStack<TraceBug *> * mainStack() {return &_mainStack;}
  static QString backTrace(const QStack<TraceBug *> * stack);
private:
  QString toString() const;

  QStack<TraceBug *> * _myStack;
  static QStack<TraceBug *> _mainStack;

  const TraceStamp * _stamp;
  QList<TraceInfo> _infos;
};

inline void TraceBug::append(const char * valName, const void * val)
{
  TraceInfo info(valName, val);
  _infos.append(info);
  qDebug() << info.toString();
}

inline void TraceBug::append(const char * valName, QVariant val)
{
  TraceInfo info(valName, val);
  _infos.append(info);
  qDebug() << info.toString();
}

} // namespace QGpCoreTools

#endif // TRACEBUG_H
