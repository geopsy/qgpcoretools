/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2010-05-24
**  Copyright: 2010-2019
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef TRACELOG_H
#define TRACELOG_H

#include "TraceInfo.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

#define TRACE_LOG \
  static TraceStamp _traceLogStamp_(__PRETTY_FUNCTION__, __LINE__); \
  TraceLog * _traceLog_=new TraceLog(&_traceLogStamp_); \
  Q_UNUSED(_traceLog_);

// Better to use ##__COUNTER__ macro but not working with gcc 4.1.3, maybe gcc 4.3
#define TRACE_LOG_N(index) \
  static TraceStamp _traceLogStamp_##index(__PRETTY_FUNCTION__, __LINE__); \
  _traceLog_=new TraceLog(&_traceLogStamp_);

#define TRACE_LOG_POINTER(val) \
  _traceLog_->append( # val,val);

#define TRACE_LOG_INT(val) \
  _traceLog_->append( # val,val);

#define TRACE_LOG_BOOL(val) \
  _traceLog_->append( # val,val);

#define TRACE_LOG_DOUBLE(val) \
  _traceLog_->append( # val,val);

#define TRACE_LOG_STRING(val) \
  _traceLog_->append( # val,val);

class TraceStamp;

class QGPCORETOOLS_EXPORT TraceLog
{
public:
  TraceLog(const TraceStamp * stamp);

  inline void append(const char * valName, const void * val);
  inline void append(const char * valName, QVariant val);

  static QVector<TraceLog *> * mainVector() {return &_mainVector;}
  static QString backTrace(const QVector<TraceLog *> * log);
  static void clearMainVector() {qDeleteAll(_mainVector);}
private:
  QString toString() const;

  static QVector<TraceLog *> _mainVector;

  const TraceStamp * _stamp;
  QList<TraceInfo> _infos;
};

inline void TraceLog::append(const char * valName, const void * val)
{
  _infos.append(TraceInfo(valName, val));
}

inline void TraceLog::append(const char * valName, QVariant val)
{
  _infos.append(TraceInfo(valName, val));
}

} // namespace QGpCoreTools

#endif // TRACELOG_H
