/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-01-29
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef MEMORYCHECKER_H
#define MEMORYCHECKER_H

#ifdef MEMORY_CHECKER
#include <new>

#include "QGpCoreToolsDLLExport.h"

extern "C++" {

  QGPCORETOOLS_EXPORT void* operator new (std::size_t sz);
  QGPCORETOOLS_EXPORT void* operator new (std::size_t sz, const char* file, int line);
  QGPCORETOOLS_EXPORT void* operator new [] (std::size_t sz);
  QGPCORETOOLS_EXPORT void* operator new [] (std::size_t sz, const char* file, int line);
  //  throw(std::bad_alloc) removed from the above declaration for valid C++17

  QGPCORETOOLS_EXPORT void operator delete (void* v) noexcept;
  QGPCORETOOLS_EXPORT void operator delete [] (void* v) noexcept;
  //  throw() removed and replaced by noexcept in the above declaration for valid C++17

} // extern "C++"

// Convenient macro definitions with code localization
#define new new(__FILE__, __LINE__)
#endif // MEMORY_CHECKER

#endif // MEMORYCHECKER_H
