/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-04-25
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "XMLTarStream.h"

namespace QGpCoreTools {

/*!
  \class XMLTarStream XMLTarStream.h
  \brief Brief description of class still missing

  Full description of class still missing
*/

/*!
  Description of constructor still missing
*/
XMLTarStream::XMLTarStream(QIODevice::OpenMode m)
    : XMLStream(_xml=new QByteArray, m)
{
  TRACE;
  _mode=m;
  _file=new Tar;
}

/*!
  Description of destructor still missing
*/
XMLTarStream::~XMLTarStream()
{
  TRACE;
  delete _file;
  delete _xml;
}

bool XMLTarStream::open(QString fileName)
{
  TRACE;
  setFileName(fileName);
  switch (_mode) {
  case QIODevice::WriteOnly: {
      QFileInfo fi(fileName);
      if(fi.exists()) { // Copy orignal file to a backup
        QFile::copy(fileName,fileName+"~");
      }
      if(_file->open(fileName, _mode)) {
        return true;
      } else {
        delete _file;
        _file=0;
        return false;
      }
    }
  case QIODevice::ReadOnly:
    if(_file->open(fileName, _mode)) {
      return true;
    } else {
      delete _file;
      _file=0;
      return false;
    }
  default:
    return false;
  }
}

} // namespace QGpCoreTools
