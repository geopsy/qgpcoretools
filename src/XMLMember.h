/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2009-06-07
**  Copyright: 2009-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef XMLMEMBER_H
#define XMLMEMBER_H

#include "XMLContext.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

class XMLClass;

class QGPCORETOOLS_EXPORT XMLMember
{
public:
  inline XMLMember(int memberID, XMLContext * context=0);
  inline XMLMember(XMLClass * child, bool temp=false, XMLContext * context=0);

  enum SpecialPublicMemberIDs {Unknown=-1, Skip=-2};

  inline void operator=(const XMLMember& o);
  inline XMLMember& operator+(int idOffset);
  inline void reset();

  bool isValidMember() {return _memberID>Unknown;}
  bool isSkipMember() {return _memberID==Skip;}
  int memberID() {return _memberID;}
  XMLClass * child() {return _child;}
  XMLContext * context(XMLContext * defaultContext) const {return _context ? _context : defaultContext;}
  inline void release();
private:
  enum SpecialPrivateMemberIDs {PermanentChild=-3, TemporaryChild=-4};
  XMLContext * _context;
  XMLClass * _child;
  int _memberID;
};

inline XMLMember::XMLMember(int memberID, XMLContext * context)
{
  _context=context;
  _child=0;
  _memberID=memberID;
}

inline XMLMember::XMLMember(XMLClass * child, bool temp, XMLContext * context)
{
  _context=context;
  _child=child;
  _memberID=temp ? TemporaryChild : PermanentChild;
}

inline void XMLMember::operator=(const XMLMember& o)
{
  _context=o._context;
  _child=o._child;
  _memberID=o._memberID;
}

inline XMLMember& XMLMember::operator+(int idOffset)
{
  if(_memberID>Unknown) _memberID+=idOffset;
  return *this;
}

inline void XMLMember::reset()
{
  _context=0;
  _child=0;
  _memberID=Unknown;
}

} // namespace QGpCoreTools

#endif // XMLMEMBER_H
