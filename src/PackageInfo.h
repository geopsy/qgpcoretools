/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-02-08
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef PACKAGEINFO_H
#define PACKAGEINFO_H

#include <QtCore>

#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT PackageInfo
{
public:
  PackageInfo(QString package,
              QString binDir,
              QString libDir,
              QString includeDir,
              QString shareDir,
              QString version,
              QString versionType,
              QString buildTime,
              QString gitCommit,
              QString distribtion,
              QString authors);
  PackageInfo(const PackageInfo& o);

  const QString & package() const {return _package;}
  const QString & binDir() const {return _binDir;}
  const QString & libDir() const {return _libDir;}
  const QString & includeDir() const {return _includeDir;}
  const QString & shareDir() const {return _shareDir;}
#ifdef Q_OS_WIN
  const QString & dllDir() const {return binDir();}
#else
  const QString & dllDir() const {return libDir();}
#endif
  const QString & version() const {return _version;}
  const QString & versionType() const {return _versionType;}
  const QString & buildTime() const {return _buildTime;}
  const QString & gitCommit() const {return _gitCommit;}
  const QString & distribution() const {return _distribution;}
  const QString & authors() const {return _authors;}

  static QString getInstallDir(QString organization, QString package, QString dir, QString defaultDir);
  static const PackageInfo * package(QString package);
  static QList<PackageInfo> * list();
private:
  QString _package;
  QString _binDir;
  QString _libDir;
  QString _includeDir;
  QString _shareDir;
  QString _version;
  QString _versionType;
  QString _buildTime;
  QString _gitCommit;
  QString _distribution;
  QString _authors;

  static QList<PackageInfo> * _list;
};

#define PACKAGE_INFO(package, PACKAGE) \
  class PACKAGE##InfoInit \
  { \
  public: \
    PACKAGE##InfoInit() { \
      PackageInfo::list()->append(PackageInfo(package, \
            PackageInfo::getInstallDir("geopsy", package, "binDir", QString::fromUtf8(PACKAGE##_BINDIR)), \
            PackageInfo::getInstallDir("geopsy", package, "libDir", QString::fromUtf8(PACKAGE##_LIBDIR)), \
            PackageInfo::getInstallDir("geopsy", package, "includeDir", QString::fromUtf8(PACKAGE##_INCDIR)), \
            PackageInfo::getInstallDir("geopsy", package, "shareDir", QString::fromUtf8(PACKAGE##_SHAREDIR)), \
            PACKAGE##_VERSION, \
            PACKAGE##_VERSION_TYPE, \
            PACKAGE##_BUILD_TIME, \
            PACKAGE##_GIT_COMMIT, \
            PACKAGE##_DISTRIBUTION, \
            QString::fromUtf8(PACKAGE##_AUTHORS))); \
    } \
  }; \
  static PACKAGE##InfoInit autoInit##PACKAGE;

} // namespace QGpCoreTools

#endif // PACKAGEINFO_H
