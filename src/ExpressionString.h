/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-04-10
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef EXPRESSIONSTRING_H
#define EXPRESSIONSTRING_H

#include "QGpCoreToolsDLLExport.h"
#include "ExpressionActions.h"
#include "ExpressionAction.h"
#include "Translations.h"

namespace QGpCoreTools {

class ExpressionParser;

class QGPCORETOOLS_EXPORT ExpressionString
{
  TRANSLATIONS("HeaderString");
public:
  ~ExpressionString() {clear();}

  bool setPattern(QString pattern, ExpressionContext * context);
  QString value();
  void clear();
private:
  bool skipString(const QChar *& ptr);

  QString _pattern;
  QList<ExpressionActions> _engines;
};

} // namespace QGpCoreTools

#endif // EXPRESSIONSTRING_H
