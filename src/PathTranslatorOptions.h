/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2017-03-03
**  Copyright: 2017-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#ifndef PATHTRANSLATOROPTIONS_H
#define PATHTRANSLATOROPTIONS_H

#include <QtCore>

#include "Translations.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT PathTranslatorOptions
  {
    TRANSLATIONS("PathTranslatorOptions")
  public:
    PathTranslatorOptions();
    ~PathTranslatorOptions();

    const QString& title() const {return _title;}
    void setTitle(const QString& t) {_title=t;}

    const QString& fileType() const {return _fileType;}
    void setFileType(const QString& f) {_fileType=f;}

    const QString& errorLog() const {return _errorLog;}
    void setErrorLog(const QString& e) {_errorLog=e;}

    const bool& forceAsk() const {return _forceAsk;}
    void setForceAsk(const bool& f) {_forceAsk=f;}

    QString userMessage(const QString& fileName) const;
  private:
    QString _title;
    QString _fileType;
    QString _errorLog;
    bool _forceAsk;
  };

} // namespace QGpCoreTools

#endif // PATHTRANSLATOROPTIONS_H

