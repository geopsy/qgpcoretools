/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-02-08
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "CoreApplication.h"
#include "File.h"

namespace QGpCoreTools {

/*!
  \class CoreApplication CoreApplication.h
  \brief Brief description of class still missing

  Full description of class still missing
*/

/*!
  Description of constructor still missing
*/
CoreApplication::CoreApplication(int & argc, char ** argv, ApplicationHelp * ( *help) (), bool reportBugs  )
    : CoreApplicationPrivate(argc, argv), QCoreApplication(argc, argv)
{
  // Qt application information
  setApplicationName(CoreApplicationPrivate::constructorApplicationName());
  setOrganizationDomain("geopsy.org");
  setOrganizationName("geopsy");
  // Install message handler
  setMessageHandler(new Message);
  // load translation if necessary
  initTranslations();

  // Check arguments
  int i, j=1;
  for(i=1; i<argc; i++) {
    QByteArray arg=argv[i];
    if(arg=="-reportbug" || arg=="-reportint") {
      reportBug();
      while (true) { // Allow debugger to be started or wait for CTRL C
        sleep(1000);
      }
    } else if(help && (arg=="--help" || arg=="-help" || arg=="-h")) {
      showHelp(i, argc, argv, help);
      ::exit(0);
    } else if(arg=="-args") {
      int max=50;
      if(checkOptionArg(i, argc, argv, false)) {
        max=atoi(argv[i]);
      }
      printArgumentLists(max);
      ::exit(0);
    } else if(arg=="-qt-plugin-paths") {
      // Need a fully constructed QCoreApplication
      printLibraryPaths();
      ::exit(0);
    } else if(arg=="--nobugreport" || arg=="-nobugreport") {
      reportBugs=false;
    } else {
      argv[j++]=argv[i];
    }
  }
  if(j < argc) {
    argv[j]=nullptr;
    argc=j;
  }

  initInternalDebugger(reportBugs);
  checkAtomicOperations();

  // Store arguments for history listing, here to avoid generic options (-h,...)
  addArgumentList(argc, argv);
}

/*!
  Description of destructor still missing
*/
CoreApplication::~CoreApplication()
{
  destructorCleanUp();
}

/*!
  
*/
void CoreApplication::reportBug()
{
  reportBugDirectly(getStdin());
}

} // namespace QGpCoreTools
