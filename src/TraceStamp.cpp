/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-10-20
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "TraceStamp.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

/*!
  \class TraceStamp TraceStamp.h
  \brief Storage for trace stamps

  Storage for trace stamps. Stamps are collected each time macro TRACE is encountered.
*/

/*!
  \fn TraceStampData::TraceStamp(const char * functionName, int lineNumber)
  Stores function name and line number.
*/

/*!
  Returns a string describing the code stamp: filename:linenumber.
*/
QString TraceStamp::toString() const
{
  QString str(_functionName);
  str+=":"+QString::number(_lineNumber);
  return str;
}


} // namespace QGpCoreTools
