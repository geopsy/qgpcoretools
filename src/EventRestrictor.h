/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2012-04-22
**  Copyright: 2012-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#ifndef EVENTRESTRICTOR_H
#define EVENTRESTRICTOR_H

#include <QtCore>

#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT EventRestrictor : public QObject
  {
    Q_OBJECT
  public:
    EventRestrictor(QObject * parent=nullptr);
    ~EventRestrictor();

    void setInterval(int ms) {_lastEvent.setInterval(ms);}
  public slots:
    void newEvent();
  private slots:
    void timeout();
  signals:
    void run();
  private:
    QTime * _chrono;
    QTimer _lastEvent;
  };

} // namespace QGpCoreTools

#endif // EVENTRESTRICTOR_H
