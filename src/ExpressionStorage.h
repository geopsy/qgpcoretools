/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-04-10
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef EXPRESSIONSTORAGE_H
#define EXPRESSIONSTORAGE_H

#include <QtCore>

#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT ExpressionStorage
{
public:
  ExpressionStorage() {}
  virtual ~ExpressionStorage() {}

  virtual QVariant value(const QString& index) const=0;
  virtual void setValue(const QString& index, const QVariant& val)=0;
  virtual bool isReadOnly() const {return false;}
};

class QGPCORETOOLS_EXPORT ExpressionVariableStorage : public ExpressionStorage
{
public:
  virtual QVariant value(const QString& index) const;
  virtual void setValue(const QString& index, const QVariant& val);
private:
  QMap<QString, QVariant> _values;
};

} // namespace QGpCoreTools

#endif // EXPRESSIONSTORAGE_H
