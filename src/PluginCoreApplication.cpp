/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-12-10
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "PluginCoreApplication.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

/*!
  \class PluginCoreApplication PluginCoreApplication.h
  \brief Brief description of class still missing

  Full description of class still missing
*/

/*!
  Description of constructor still missing
*/
PluginCoreApplication::PluginCoreApplication()
    : CoreApplicationPrivate()
{
#ifdef DO_MEMORY_CHECK
  // Disable memory checker
  MemoryCheckerPrivate::setEnabled(false);
#endif
  // Install message handler
  setMessageHandler(new Message);
}

/*!
  Description of destructor still missing
*/
PluginCoreApplication::~PluginCoreApplication()
{
  destructorCleanUp();
}

} // namespace QGpCoreTools
