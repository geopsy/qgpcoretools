/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2011-01-12
**  Copyright: 2011-2019
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef LOCALPOINTER_H
#define LOCALPOINTER_H

#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  template<class T>
  class QGPCORETOOLS_EXPORT LocalPointer
  {
  public:
    LocalPointer(T * p) {_p=p;}
    ~LocalPointer() {delete _p;}
  private:
    T * _p;
  };

  #define LOCAL_POINTER(T, p) \
    LocalPointer<T> lp_##T##_##p(p); \
    Q_UNUSED(lp_##T##_##p);

} // namespace QGpCoreTools

#endif // LOCALPOINTER_H
