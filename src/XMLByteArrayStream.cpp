/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-04-25
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "XMLByteArrayStream.h"
#include "CoreApplication.h"
#include "Global.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

/*!
  \class XMLByteArrayStream XMLByteArrayStream.h
  \brief Brief description of class still missing

  Store XMLClass information in a QByteArray with format:

  QString        Tag "XMLClass"
  qint32         Version (currently 0)
  File1
    QString      File name
    QByteArray   File content
  File2
    QString      File name
    QByteArray   File content
  ...
  FileN
    QString      File name
    QByteArray   File content
*/

XMLByteArrayStream::XMLByteArrayStream(QIODevice::OpenMode m)
 : XMLStream(_xml=new QByteArray, m)
{
  TRACE;
  _mode=m;
  _lastPos=0;
  if(_mode==QIODevice::WriteOnly) {
    QDataStream s(&_data, QIODevice::WriteOnly);
    QString tag("XMLClass");
    s << tag;
    qint32 version=0;
    s << version;
  }
}

XMLByteArrayStream::~XMLByteArrayStream()
{
  TRACE;
  flush();
  delete _xml;
}

bool XMLByteArrayStream::open(QByteArray data)
{
  TRACE;
  ASSERT (_mode==QIODevice::ReadOnly);
  _data=data;
  QDataStream s(_data);
  QString tag;
  s >> tag;
  if(tag!="XMLClass") {
    App::log(tr("Wrong tag, not a XMLClass byte array\n") );
    return false;
  }
  qint32 version;
  s >> version;
  if(version>0) {
    App::log(tr("Wrong version(%1), current version is %2\n").arg(version).arg(0) );
    return false;
  }
  _lastPos=s.device()->pos();
  return true;
}

void XMLByteArrayStream::addFile(QString fileName, const QByteArray& data)
{
  TRACE;
  QDataStream s(&_data, QIODevice::Append);
  s << fileName;
  _files.insert(fileName, data);
  s << data;
}

bool XMLByteArrayStream::file(QString fileName, QByteArray& data)
{
  TRACE;
  QMap<QString, QByteArray>::iterator it=_files.find(fileName);
  if(it==_files.end()) {
    QDataStream s(&_data, QIODevice::ReadOnly);
    s.device()->seek(_lastPos);
    while(_lastPos<_data.size()) {
      QString readFileName;
      s >> readFileName;
      s >> data;
      _files.insert(readFileName, data);
      _lastPos=s.device()->pos();
      if(readFileName==fileName) return true;
    }
    return false;
  } else {
    data=it.value();
    return true;
  }
}

} // namespace QGpCoreTools
