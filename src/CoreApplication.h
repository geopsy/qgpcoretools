/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-02-08
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef COREAPPLICATION_H
#define COREAPPLICATION_H


#include "QGpCoreToolsDLLExport.h"
#include "CoreApplicationPrivate.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT CoreApplication : public CoreApplicationPrivate, public QCoreApplication
{
  TRANSLATIONS("CoreApplication")
public:
  CoreApplication(int & argc, char ** argv, ApplicationHelp * ( *help) (), bool reportBugs=true);
  ~CoreApplication();

  static CoreApplicationPrivate * instance() {return CoreApplicationPrivate::instance();}
  virtual bool hasGui() const {return false;}
private:
  void reportBug();
};

} // namespace QGpCoreTools

#endif // COREAPPLICATION_H
