/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2018-05-28
**  Copyright: 2018-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#ifndef MUTEX_H
#define MUTEX_H

#include <QtCore>

#include "QGpCoreToolsDLLExport.h"

#ifdef LOG_MUTEX
#define MUTEX_VERBOSITY 5
#define MUTEX_LOCK lock(__FILE__, __LINE__)
#else
#define MUTEX_LOCK lock()
#endif

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT Mutex : public QMutex
  {
  public:
    Mutex(RecursionMode mode = NonRecursive) : QMutex(mode) {}

#ifdef LOG_MUTEX
    void lock(const char* file, int line);
    void lock();
#endif
  };

} // namespace QGpCoreTools

#endif // MUTEX_H

