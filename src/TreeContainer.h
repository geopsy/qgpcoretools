/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2011-05-16
**  Copyright: 2011-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#ifndef TREECONTAINER_H
#define TREECONTAINER_H

#include "TreeItem.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT TreeContainer : public TreeItem
  {
  public:
    TreeContainer(TreeContainer * parent=0) : TreeItem(parent) {}
    virtual ~TreeContainer();

    TreeItem * childAt(int index) {return _children.at(index);}
    const TreeItem * childAt(int index) const {return _children.at(index);}
    int childrenCount() const {return _children.count();}
    int rank() const {return TreeItem::rank();}
    int rank(const TreeItem * item) const {return _children.indexOf(const_cast<TreeItem *>(item));}
    virtual void setRank(int r) {TreeItem::setRank(r);}
    virtual bool isContainer() const {return true;}

    typedef QList<TreeItem *>::iterator iterator;
    typedef QList<TreeItem *>::const_iterator const_iterator;
    iterator begin() {return _children.begin();}
    iterator end() {return _children.end();}
    const_iterator begin() const {return _children.begin();}
    const_iterator end() const {return _children.end();}

    int containerCount() const;
    TreeContainer * containerAt(int index) const;
    int containerIndex() const;

    bool canTakeChildren(const TreeContainer * o) const;
    void takeChildren(TreeContainer * o);
  protected:
    virtual void xml_writeChildren(XML_WRITECHILDREN_ARGS) const;
  private:
    friend class TreeItem;

    void setRank(int oldr, int newr);
    void addChild(TreeItem * item) {_children.append(item);}
    bool takeChild(TreeItem * item);

    QList<TreeItem *> _children;
  };

} // namespace QGpCoreTools

#endif // TREECONTAINER_H
