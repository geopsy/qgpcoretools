/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-02-08
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/


#include "PackageInfo.h"
#include "Trace.h"

namespace QGpCoreTools {

QList<PackageInfo> * PackageInfo::_list=0;

/*!
  \class PackageInfo PackageInfo.h
  \brief Brief description of class still missing

  Full description of class still missing
*/

/*!
  Description of constructor still missing
*/
PackageInfo::PackageInfo(QString package,
                         QString binDir,
                         QString libDir,
                         QString includeDir,
                         QString shareDir,
                         QString version,
                         QString versionType,
                         QString buildTime,
                         QString gitCommit,
                         QString distribution,
                         QString authors)
{
  _package=package;
  _binDir=binDir;
  _libDir=libDir;
  _includeDir=includeDir;
  _shareDir=shareDir;
  _version=version;
  _versionType=versionType;
  _buildTime=buildTime;
  _gitCommit=gitCommit;
  _distribution=distribution;
  _authors=authors;
}

/*!
  Copy constructor
*/
PackageInfo::PackageInfo(const PackageInfo& o)
{
  _package=o._package;
  _binDir=o._binDir;
  _libDir=o._libDir;
  _includeDir=o._includeDir;
  _shareDir=o._shareDir;
  _version=o._version;
  _versionType=o._versionType;
  _buildTime=o._buildTime;
  _gitCommit=o._gitCommit;
  _distribution=o._distribution;
  _authors=o._authors;
}

QString PackageInfo::getInstallDir(QString organization, QString package, QString dir, QString defaultDir)
{
  QSettings reg(organization, package);
  reg.beginGroup("installpaths");
  if(reg.contains(dir)) {
    return reg.value(dir).toString();
  } else {
    return defaultDir;
  }
}

QList<PackageInfo> * PackageInfo::list()
{
  if(!_list) _list=new QList<PackageInfo>;
  return _list;
}

const PackageInfo * PackageInfo::package(QString package)
{
  for(QList<PackageInfo>::iterator it=PackageInfo::_list->begin();it!=PackageInfo::_list->end(); it++ ) {
    if(package==it->package()) {
      return &(*it);
    }
  }
  return 0;
}


} // namespace QGpCoreTools
