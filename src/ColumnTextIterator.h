/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2011-01-21
**  Copyright: 2011-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#ifndef COLUMNTEXTITERATOR_H
#define COLUMNTEXTITERATOR_H

#include "QGpCoreToolsDLLExport.h"
#include "ColumnTextParser.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT ColumnTextIterator
  {
  public:
    inline ColumnTextIterator(const ColumnTextParser * parser);
    inline ColumnTextIterator(const ColumnTextIterator& o);
    ~ColumnTextIterator() {}

    inline void setCurrentSection(int index);
    int currentSection() const {return _section;}
    int currentRow() const {return _row;}

    void nextRow() {_row++;}
    bool atSectionEnd();
    bool atEnd() const;

    const ColumnTextParser * parser() const {return _parser;}
  private:
    const ColumnTextParser * _parser;
    int _section, _row, _endSectionRow;
  };

  inline ColumnTextIterator::ColumnTextIterator(const ColumnTextParser * parser)
  {
    _parser=parser;
    _section=0;
    _row=0;
    _endSectionRow=_parser->sectionEndRow(_section);
  }

  inline ColumnTextIterator::ColumnTextIterator(const ColumnTextIterator& o)
  {
    _parser=o._parser;
    _section=o._section;
    _row=o._row;
    _endSectionRow=o._endSectionRow;
  }

  inline void ColumnTextIterator::setCurrentSection(int index)
  {
    _section=index;
    _row=_parser->sectionBeginRow(_section);
    _endSectionRow=_parser->sectionEndRow(_section);
  }

  inline bool ColumnTextIterator::atSectionEnd()
  {
    if(_row<_endSectionRow) {
      return false;
    } else {
      _section++;
      _endSectionRow=_parser->sectionEndRow(_section);
      return true;
    }
  }

  inline bool ColumnTextIterator::atEnd() const
  {
    if(_row<_parser->rowCount()) {
      return false;
    } else {
      return true;
    }
  }

} // namespace QGpCoreTools

#endif // COLUMNTEXTITERATOR_H
