/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-04-10
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "ExpressionStorage.h"
#include "Trace.h"

namespace QGpCoreTools {

/*!
  \class ExpressionStorage ExpressionStorage.h
  \brief Brief description of class still missing

  Full description of class still missing
*/

QVariant ExpressionVariableStorage::value(const QString& index) const
{
  TRACE;
  QMap<QString, QVariant>::const_iterator it=_values.find(index);
  if(it!=_values.end()) {
    return it.value();
  } else {
    return QVariant();
  }
}

void ExpressionVariableStorage::setValue(const QString& index, const QVariant& val)
{
  TRACE;
  _values[index]=val;
}

} // namespace QGpCoreTools
