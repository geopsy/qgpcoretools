/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2004-03-10
**  Copyright: 2004-2019
**    Marc Wathelet
**    Marc Wathelet (ULg, Liège, Belgium)
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef FILE_H
#define FILE_H

#include <zlib.h>
#include <QtCore>

#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT File
  {
  public:
    static quint32 crc32(const QString& fileName, qint64 maxSize, bool *ok=0);

    static QStringList expand(const QStringList& fileNames);
    static QStringList expand(const QString& fileName);
    static QString uniqueName(QString fileName, const QDir& d);
    static QMap<QString, QString> fileMap(const QDir &d);
    static void fileMap(const QDir& d, QMap<QString, QString>& map);
    static QString fileName(QString filePath, QChar sep);

    static char * readAll(const char * fileName);
    static bool readLine(char *& buf, int& bufLen, gzFile f, bool delEOL=false);
    static bool readLine(char *& buf, int& bufLen, FILE * f, bool delEOL=false);
    static QString readLine(FILE * f, bool delEOL=false);
    static QString readLine(bool delEOL);
    static void readBreakLine(char *& buf, int& bufLen, FILE * f);
    static bool readCleanUp(char * buf, bool returnValue);
    static void readLineNoComments(char * buf, int& bufLen, FILE *  f, QString * comments);
    static QString readLineNoComments(QTextStream& s, QString * comments=nullptr, int * lineNumber=nullptr);
    static inline bool readLine(QTextStream& s, QString& line, const QString& pattern=QString::null);
    static inline bool isDataLine(const QString& line, const QString& pattern=QString::null);

    static char * stripWhiteSpace(char * buf);
    static const char * stripWhiteSpace(const char * buf, int& len);
    static char * nextField(char *& buf, const char * sep);
    static void getKeyValue(const char * fileNAme, const char * varName, QString& value);
    // Util function for library or plugin management
    static QStringList getLibList(QStringList pathList);
    // Util for reading fixed file formats
    static qint32 fromBigEndian(qint32 v);
    static qint32 fromLittleEndian(qint32 v);
    static qint16 fromBigEndian(qint16 v);
    static qint16 fromLittleEndian(qint16 v);
  private:
    static QStringList expand(QDir d, QStringList filters, QString levelName, int level);
  };

  inline bool File::isDataLine(const QString& line, const QString& pattern)
  {
    return !line.isEmpty() &&
           line[0]!='#' &&
           (pattern.isEmpty() || line.contains(pattern));
  }

  inline bool File::readLine(QTextStream& s, QString& line, const QString& pattern)
  {
    line=s.readLine();
    return isDataLine(line, pattern);
  }

} // namespace QGpCoreTools

#endif // FILE_H
