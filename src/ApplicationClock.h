/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2010-09-09
**  Copyright: 2010-2019
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef APPLICATIONCLOCK_H
#define APPLICATIONCLOCK_H

#include <sys/time.h>
#include <QtCore>

#include "QGpCoreToolsDLLExport.h"

// Approximately 584492 years...
// Add suffix ULL to have a long long constant (required by 32 bit systems)
#define MAXIMUM_APPLICATION_LIFETIME 0xFFFFFFFFFFFFFFFFULL

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT ApplicationClock
  {
  public:
    ApplicationClock();

    static quint64 elapsed() {return _instance.elapsedInternal();}
  private:
    quint64 elapsedInternal();
    static ApplicationClock _instance;
    quint64 _reference;
  };

} // namespace QGpCoreTools

#endif // APPLICATIONCLOCK_H
