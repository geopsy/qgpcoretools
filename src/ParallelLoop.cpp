/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-06-20
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "ParallelLoop.h"
#include "CoreApplication.h"
#include "Global.h"
#include "Trace.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

  /*!
    \class LoopTask ParallelLoop.h
    \brief Task of a parallel loop
  */

  /*!
    To check affinities:
    for i in $(pgrep geopsy-fk);do ps -mo pid,tid,fname,user,psr -p $i;done | tail -n 40
  */
  void LoopTask::run()
  {
    TRACE;
    if(_affinity>=0) {
#ifdef Q_OS_LINUX
      cpu_set_t cpuset;
      pthread_t thread=pthread_self();
      CPU_ZERO(&cpuset);
      CPU_SET(_affinity, &cpuset);
      if(pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset)!=0) {
        App::log(tr("Thead affinity mask cannot be set to %1, skipping CPU affinity optimization\n").arg(_affinity));
      }
#endif
    }
    while(true) {
      if(terminated()) break;
      _indexMutex->MUTEX_LOCK;
      int i=*_index;
      (*_index)++;
      _indexMutex->unlock();
      if(i<_endIndex) {
        run(i);
      } else {
        break;
      }
    }
  }

  /*!
    \fn int LoopTask::endIndex() const
    Returns the final index of the loop. It is the same for all children LoopTask.
    This might be usefull to revert the order of execution in run(int index) implementations.
  */

  /*!
    \class ParallelLoop ParallelLoop.h
    \brief Class that helps to parallelize loops
  */

  /*!
    Initialize a loop. The parameters \a param will be provided as a pointer to each task.
  */
  ParallelLoop::ParallelLoop(QObject * parent)
    : QObject(parent)
  {
  }

  void ParallelLoop::terminate()
  {
    TRACE;
    for(QMap<LoopTask *,TaskInfo>::iterator it=_tasks.begin(); it!=_tasks.end(); it++) {
      if(!it.key()->terminated()) it.key()->terminate();
    }
  }

  void ParallelLoop::taskFinished(LoopTask * t)
  {
    TRACE;
    if(!t) {
      t=static_cast<LoopTask *>(sender());
    }
    QMap<LoopTask *,TaskInfo>::iterator it=_tasks.find(t);
    if(it!=_tasks.end()) {
      emit statusChanged(it.value().index, tr("Finished"));
      emit progressChanged(it.value().index, it.value().maximumProgress);
      _tasks.erase(it);
      delete t;
      if(_tasks.isEmpty()) {
        emit finished();
      }
    }
  }

  void ParallelLoop::waitFinished()
  {
    while(!_tasks.isEmpty()) {
      App::sleep(500);
      QCoreApplication::processEvents();
    }
  }

  inline LoopTask * ParallelLoop::newTask(int iEnd)
  {
    LoopTask * t=newTask();
    connect(t, SIGNAL(finished()), this, SLOT(taskFinished()));
    connect(t, SIGNAL(statusChanged(QString)), this, SLOT(statusChanged(QString)));
    connect(t, SIGNAL(progressInit(int)), this, SLOT(progressInit(int)));
    connect(t, SIGNAL(progressChanged(int)), this, SLOT(progressChanged(int)));
    t->setIndex(&_index, &_indexMutex);
    t->setEndIndex(iEnd);
    return t;
  }

  /*!
    Start loop from \a iStart to \a iEnd-1
  */
  void ParallelLoop::start(int iStart, int iEnd, bool forceParallel)
  {
    TRACE;
    int nThreads=idealThreadCount(iEnd-iStart+1);
    _index=iStart;
    LoopTask * t;
    TaskInfo infos;
    if(nThreads<=1 && !forceParallel) {
      App::log(1, tr("Running loop without parallel threads\n"));
      t=newTask(iEnd);
      infos.index=0;
      infos.maximumProgress=0;
      _tasks.insert(t, infos);
      t->run();
      taskFinished(t);
    } else {
      App::log(1, tr("Running loop with %1 parallel threads\n").arg(nThreads));
      int affinityShift=0;
      for(int i=0; i<nThreads; i++) {
        //App::log("starting new thread\n");
        t=newTask(iEnd);
        t->setObjectName("parallelLoop");
        infos.index=i;
        infos.maximumProgress=0;
        t->setObjectName(QString("LoopTask_%1").arg(_tasks.count()));
        _tasks.insert(t, infos);
        t->setIndex(&_index, &_indexMutex);
        if(!Thread::cpuAffinities().isEmpty()) {
          t->setAffinity(Thread::cpuAffinities().at(i-affinityShift));
          APP_LOG(2, tr("Assign affinity %1 to task %2\n").arg(t->affinity()).arg(t->objectName()))
          if(i==Thread::cpuAffinities().count()-1) {
            affinityShift+=Thread::cpuAffinities().count();
          }
        }
        t->start();
        // for debug run them sequentially
        //t->run();
        //taskFinished(t);
      }
    }
  }

  void ParallelLoop::statusChanged(QString msg)
  {
    TRACE;
    LoopTask * t=qobject_cast<LoopTask *>(sender());
    QMap<LoopTask *,TaskInfo>::iterator it=_tasks.find(t);
    if(it!=_tasks.end()) {
      emit statusChanged(it.value().index, msg);
    }
  }

  void ParallelLoop::progressChanged(int value)
  {
    TRACE;
    LoopTask * t=qobject_cast<LoopTask *>(sender());
    QMap<LoopTask *,TaskInfo>::iterator it=_tasks.find(t);
    if(it!=_tasks.end()) {
      emit progressChanged(it.value().index, value);
    }
  }

  void ParallelLoop::progressInit(int maximumValue)
  {
    TRACE;
    LoopTask * t=qobject_cast<LoopTask *>(sender());
    QMap<LoopTask *,TaskInfo>::iterator it=_tasks.find(t);
    if(it!=_tasks.end()) {
      // Store maximum value for taskFinished()
      if(it.value().maximumProgress!=maximumValue) {
        it.value().maximumProgress=maximumValue;
        emit progressInit(it.value().index, maximumValue);
      }
    }
  }

} // namespace QGpCoreTools
