/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2005-08-17
**  Copyright: 2005-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef TRANSLATIONS_H
#define TRANSLATIONS_H

#include <QtCore>

namespace QGpCoreTools {

inline QString tr(const char * s, const char * comments=nullptr)
{
  return QCoreApplication::translate("global", s, comments);
}

} // namespace QGpCoreTools

/*
  Use this MACRO in all class definitions where there are no QObject inheritance
*/
#define TRANSLATIONS(context) \
public: \
  static QString tr(const char * s, const char * comments=nullptr) {return QCoreApplication::translate(context, s, comments);}

#endif // TRANSLATIONS_H
