/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2006-06-08
**  Copyright: 2006-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef PATHTRANSLATOR_H
#define PATHTRANSLATOR_H

#include "PathTranslatorOptions.h"
#include "QGpCoreToolsDLLExport.h"
#include "Translations.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT PathTranslator : private QMap<QString, QString>
  {
    TRANSLATIONS("PathTranslator")
  public:
    PathTranslator() {}
    ~PathTranslator() {}

    QString translate(const QString& originalFile, const PathTranslatorOptions& options);
  private:
    void addPath(QString obsoletePath, QString newPath, QChar sep);
    static bool fileExists(QFileInfo& fi);
    QString platformIndependantPath(QString path);
    QString askPath (const PathTranslatorOptions& options, const QFileInfo& file,
                     QChar sep, const QString& directory=QString::null);
    static QChar pathSeparator(QString path);
    static bool isRoot(QString path);
  };

} // namespace QGpCoreTools

#endif // PATHTRANSLATOR_H
