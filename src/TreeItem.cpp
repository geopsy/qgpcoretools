/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2006-11-24
**  Copyright: 2006-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "CoreApplication.h"
#include "TreeItem.h"
#include "TreeContainer.h"
#include "Global.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

  /*!
    \class TreeItem TreeItem.h
    \brief Brief description of class still missing

    Full description of class still missing
  */

  const QString TreeItem::xmlTreeItemTag="TreeItem";

  /*!
    Description of constructor still missing
  */
  TreeItem::TreeItem(TreeContainer * parent)
  {
    TRACE;
    // As we are in constructor, parent cannot be this and if this is a container
    // it has still no children => no circular reference possible. We skip all
    // tests performed by setParent().
    if(parent) {
      parent->addChild(this);
      _parent=parent;
    } else {
      _parent=0;
    }
  }

  /*!
    Returns true if \a parent can be a valid parent.
    Circular references are checked.
  */
  bool TreeItem::isValidParent(const TreeContainer * parent) const
  {
    TRACE;
    while(parent) {
      if(static_cast<const TreeItem *>(parent)==this) {
        return false;
      }
      parent=parent->parent();
    }
    return true;
  }

  /*!
    Sets parent to \a parent. \a parent can be null.
    Circular references are not checked. It is safe to use isValidParent()
    before changing parent.
  */
  void TreeItem::setParent(TreeContainer * parent)
  {
    TRACE;
    if(_parent) {
      _parent->takeChild(this);
    }
    if(parent) {
      parent->addChild(this);
    }
    _parent=parent;
  }

  /*!
    Return the index of this item in children list of its parent.
    Without parents this function returns 0.
  */
  int TreeItem::rank() const
  {
    TRACE;
    if(_parent)
      return _parent->rank(this);
    else
      return 0;
  }

  /*!
    Set index of this item in children list of its parent.
    Without parents this function does nothing. If \a r is larger than
    the number of children in its parent, the rank is set to number
    of children less 1.
  */
  void TreeItem::setRank(int r)
  {
    TRACE;
    if(_parent) {
      int oldr=_parent->rank(this);
      ASSERT(oldr>=0); // The contrary would mean a tree corruption
      if(oldr!=r) {
        _parent->setRank(oldr, r);
      }
    }
  }

} // namespace QGpCoreTools
