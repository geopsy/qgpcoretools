/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2018-05-28
**  Copyright: 2018-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#ifndef READWRITELOCK_H
#define READWRITELOCK_H

#include <QtCore>

#include "QGpCoreToolsDLLExport.h"
#include "Mutex.h"

#ifdef LOG_MUTEX
#define MUTEX_READ_LOCK lockForRead(__FILE__, __LINE__)
#define MUTEX_WRITE_LOCK lockForRead(__FILE__, __LINE__)
#else
#define MUTEX_READ_LOCK lockForRead()
#define MUTEX_WRITE_LOCK lockForRead()
#endif

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT ReadWriteLock : public QReadWriteLock
  {
  public:
    ReadWriteLock(RecursionMode mode = NonRecursive) : QReadWriteLock(mode) {}

#ifdef LOG_MUTEX
    void lockForRead(const char* file, int line);
    void lockForWrite(const char* file, int line);
    void lockForRead();
    void lockForWrite();
#endif
  };

} // namespace QGpCoreTools

#endif // READWRITELOCK_H

