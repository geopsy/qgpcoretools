/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2017-03-03
**  Copyright: 2017-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#include "PathTranslatorOptions.h"
#include "Trace.h"

namespace QGpCoreTools {

  /*!
    \class PathTranslatorOptions PathTranslatorOptions.h
    \brief Brief description of class still missing

    Full description of class still missing
  */

  /*!
    Description of constructor still missing
  */
  PathTranslatorOptions::PathTranslatorOptions()
  {
    TRACE;
    _forceAsk=false;
    _errorLog=tr("File '%1' does not exist or cannot be accessed.\n");
  }

  /*!
    Description of destructor still missing
  */
  PathTranslatorOptions::~PathTranslatorOptions()
  {
    TRACE;
  }

  /*!
    Return the user message before asking to provide the new path
  */
  QString PathTranslatorOptions::userMessage(const QString& fileName) const
  {
    TRACE;
    QString msg;
    if(_errorLog.contains("%1")) {
      msg=_errorLog.arg(fileName);
    } else {
      msg=_errorLog;
    }
    if(!msg.endsWith("\n")) {
      msg+="\n";
    }
    msg+=tr("The path may have changed. Would you like to manually select its new location?");
    return msg;
  }


} // namespace QGpCoreTools

