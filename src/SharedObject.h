/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2006-05-17
**  Copyright: 2006-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef SHAREDOBJECT_H
#define SHAREDOBJECT_H

#include <QtCore>

#include "Global.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT SharedObject
{
public:
  SharedObject() {_referenceCount=0;}
  SharedObject(const SharedObject&) {_referenceCount=0;}
  inline virtual ~SharedObject();

  void operator=(const SharedObject&) {}

  void addReference() {_referenceCount.fetchAndAddOrdered(1);}
  bool removeReference() {return _referenceCount.fetchAndAddOrdered(-1)<=1;}
  int referenceCount() const {return _referenceCount.fetchAndAddOrdered(0);}

  static inline void removeReference(SharedObject * object);
private:
  mutable QAtomicInt _referenceCount;
};

inline void SharedObject::removeReference(SharedObject * object)
{
  if(object->removeReference()) delete object;
}

inline SharedObject::~SharedObject()
{
#if(QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
  ASSERT(_referenceCount.load()<=0);
#else
  ASSERT(_referenceCount<=0);
#endif
}

} // namespace QGpCoreTools

#endif // SHAREDOBJECT_H
