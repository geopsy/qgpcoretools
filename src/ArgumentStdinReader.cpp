/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-12-11
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "ArgumentStdinReader.h"
#include "CoreApplication.h"
#include "Trace.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

  /*!
    \class ArgumentStdinReader ArgumentStdinReader.h
    \brief Brief description of class still missing

    Full description of class still missing
  */
  ArgumentStdinReader::~ArgumentStdinReader()
  {
  }

  /*!
    Reads all files listed in arguments \a argc and \a argv.
    If there is no arguments, the stdin is read.
    If an error occurs, it returns false.
  */
  bool ArgumentStdinReader::read(int argc, char ** argv, bool ignoreStdin)
  {
    TRACE;
    CoreApplication::instance()->debugUserInterrupts(false);
    QTextStream sIn;
    int i=1;
    QFile * f;
    if(!ignoreStdin && i==argc) {
      f=new QFile();
      f->open(stdin, QIODevice::ReadOnly);
      sIn.setDevice(f);
    } else {
      f=nullptr;
    }
    while(true) {
      if(f && sIn.atEnd()) {
        delete f;
        f=nullptr;
        // Closing file is not necessary for stdin but it does not hurt
      }
      if(!f) {
        if(i<argc) {
          f=new QFile(argv[i]);
          if(!f->open(QIODevice::ReadOnly)) {
            App::log(tr("Cannot open file '%1' for reading.\n").arg(argv[i]));
            delete f;
            CoreApplication::instance()->debugUserInterrupts(true);
            return false;
          } else {
             App::log(tr("Reading file '%1'...\n").arg(argv[i]));
          }
          _fileNames.append(argv[i]);
          sIn.setDevice(f);
          i++;
        } else {
          delete f;
          CoreApplication::instance()->debugUserInterrupts(true);
          return true;
        }
      }
      if(!parse(sIn)) {
        delete f;
        CoreApplication::instance()->debugUserInterrupts(true);
        return false;
      }
    }
  }

  bool ArgumentStdinReader::read(QStringList fileNames)
  {
    CoreApplication::instance()->debugUserInterrupts(true);
    QTextStream sIn;
    QFile * f=nullptr;
    int i=0;
    while(true) {
      if(f && sIn.atEnd()) {
        delete f;
        f=nullptr;
        // Closing file is not necessary for stdin but it does not hurt
      }
      if(!f) {
        if(i<fileNames.count()) {
          QString name=fileNames.at(i++);
          f=new QFile(name);
          if(!f->open(QIODevice::ReadOnly)) {
            App::log(tr("Cannot open file '%1' for reading.\n").arg(name));
            delete f;
            CoreApplication::instance()->debugUserInterrupts(true);
            return false;
          } else {
             App::log(tr("Reading file '%1'...\n").arg(name));
          }
          _fileNames.append(name);
          sIn.setDevice(f);
        } else {
          delete f;
          CoreApplication::instance()->debugUserInterrupts(true);
          return true;
        }
      }
      if(!parse(sIn)) {
        delete f;
        CoreApplication::instance()->debugUserInterrupts(true);
        return false;
      }
    }
  }


} // namespace QGpCoreTools
