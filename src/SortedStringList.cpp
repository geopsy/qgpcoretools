/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2015-01-26
**  Copyright: 2015-2019
**    Marc Wathelet (ISTerre, Grenoble, France)
**
***************************************************************************/

#include "SortedStringList.h"

namespace QGpCoreTools {

  /*!
    \class SortedStringList SortedStringList.h
    \brief A sorted string list for a fast binary search

    Full description of class still missing
  */

  void SortedStringList::insert(const QString& s)
  {
    QStringList::iterator it=qLowerBound(begin(), end(), s);
    if(it==end()) {
      append(s);
    } else if(*it!=s) {
      QStringList::insert(it, s);
    }
  }

} // namespace QGpCoreTools
