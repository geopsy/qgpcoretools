/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2010-09-11
**  Copyright: 2010-2019
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef STRINGSTREAM_H
#define STRINGSTREAM_H

#include "AbstractStream.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT StringStream : public AbstractStream
  {
  public:
    StringStream(QString * str) : AbstractStream() {_s=str;}

    virtual ClassId classId() const {return StringId;}
  protected:
    virtual void sendToStream(const QString& val) {*_s+=val;}
    virtual void flushStream() {}
  private:
    QString * _s;
  };

} // namespace QGpCoreTools

#endif // STRINGSTREAM_H
