/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2010-09-09
**  Copyright: 2010-2019
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "ApplicationClock.h"

namespace QGpCoreTools {

  /*!
    \class ApplicationClock ApplicationClock.h
    \brief Brief description of class still missing

    From experience, clock() does not return a precision better than seconds.
    This clock returns the number of microseconds since the startup of the application.
    It is used for instance for Cache management.
  */

  ApplicationClock ApplicationClock::_instance;

  /*!
    Initialize the reference time, i.e. the startup time.
  */
  ApplicationClock::ApplicationClock()
  {
    timeval now;
    gettimeofday(&now, nullptr);
    _reference=now.tv_usec;
    _reference+=now.tv_sec*1000000;
  }

  /*!
    \fn quint64 ApplicationClock::elapsed()

    Returns the number of microseconds elapsed since application startup.
    The maximum life time of the application must not exceed 584492 years.
  */

  /*!
    Internal use only

    \sa elapsed()
  */
  quint64 ApplicationClock::elapsedInternal()
  {
    timeval now;
    gettimeofday(&now, nullptr);
    quint64 us=now.tv_usec;
    us+=now.tv_sec*1000000;
    us-=_reference;
    return us;
  }

} // namespace QGpCoreTools
