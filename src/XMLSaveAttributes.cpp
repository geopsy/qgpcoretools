/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-11-09
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "XMLSaveAttributes.h"
#include "Trace.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

  /*!
    \class XMLSaveAttributes XMLSaveAttributes.h
    \brief Brief description of class still missing

    Full description of class still missing
  */

  /*!
    \fn QString& XMLSaveAttributes::add(QString key, QString value)
    Add \a key associated to \a value in the list of attributes.
    It returns a reference to the value, usefull if a null \a value is passed
    for future modifications.
  */

  /*!
    Returns the value corresponding to \a key. If \a key does not exist, a null string.
    Slow lookup, the complete list of attributes may be traversed.
  */
  QString XMLSaveAttributes::value(const QString& key)
  {
    for(const_iterator it=begin(); it!=end();it++) {
      if(it->key()==key) return it->value();
    }
    return QString::null;
  }

  /*!
    Returns true if it has all attributes of \a o. If \a o has no values for keys
    only the existence of the keys is checked (the values are not considered).
  */
  bool XMLSaveAttributes::isSimilar(const XMLSaveAttributes& o) const
  {
    TRACE;
    // Cache available keys in a map
    QMap<QString, QString> map;
    for(const_iterator it=begin(); it!=end();it++) {
      map.insert(it->key(), it->value());
    }
    QMap<QString, QString>::iterator itmap;
    for(const_iterator it=o.begin(); it!=o.end();it++) {
      itmap=map.find(it->key());
      if(itmap==map.end() || ( !it->value().isEmpty() && itmap.value()!=it->value())) {
        return false;
      }
    }
    return true;
  }

  QString XMLSaveAttributes::toString() const
  {
    TRACE;
    QString tmp;
    for(const_iterator it=begin();it!=end();it++) {
      tmp += " ";
      tmp += it->key();
      tmp += "=\"";
      tmp += it->value();
      tmp += "\"";
    }
    return tmp;
  }

} // namespace QGpCoreTools
