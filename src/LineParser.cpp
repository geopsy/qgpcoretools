/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2009-03-15
**  Copyright: 2009-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#include "LineParser.h"
#include "Trace.h"
#include "MemoryChecker.h"

namespace QGpCoreTools {

  /*!
    \class LineParser LineParser.h
    \brief Brief description of class still missing

    Full description of class still missing
  */

  /*!
    Description of constructor still missing
  */
  LineParser::LineParser()
  {
    TRACE;
    _ptr=0;
    _delimiters=StringSection::defaultSeparators;
    _skipEmpty=true;
    _quotes=false;
  }

  /*!
    Description of constructor still missing
  */
  LineParser::LineParser(const QString& str)
  {
    TRACE;
    _str=str;
    _strSection.set(_str);
    _ptr=0;
    _delimiters=StringSection::defaultSeparators;
    _skipEmpty=true;
    _quotes=false;
  }

  /*!
    Description of constructor still missing
  */
  LineParser::LineParser(const StringSection& str)
  {
    TRACE;
    _strSection=str;
    _ptr=0;
    _delimiters=StringSection::defaultSeparators;
    _skipEmpty=true;
    _quotes=false;
  }

  void LineParser::setString(const QString& str)
  {
    TRACE;
    _str=str;
    _strSection.set(_str);
    _values.clear();
    _ptr=0;
  }

  void LineParser::setString(const StringSection& str)
  {
    TRACE;
    _strSection=str;
    _values.clear();
    _ptr=0;
  }

  /*!
    Parses all and returns number of items
  */
  int LineParser::count()
  {
    TRACE;
    int index=_values.count();
    while(parse(index)!=StringSection::null) {
      index++;
    }
    return _values.count();
  }

  /*!
    Returns all available columns between \a indexBegin and \a indexEnd.
    \a ok is set to false only if no column is available.
  */
  QString LineParser::toString(int indexBegin, int indexEnd, bool& ok)
  {
    const StringSection& vBegin=value(indexBegin);
    if(!vBegin.isValid()) {
      ok=false;
      return QString::null;
    }
    const StringSection& vEnd=value(indexEnd);
    if(vEnd.isValid()) {
      return StringSection(vBegin.begin(), vEnd.end()).toString();
    } else {
      return StringSection(vBegin.begin(), _values.last().end()).toString();
    }
  }

  const StringSection& LineParser::parse(int index)
  {
    TRACE;
    StringSection field;
    for(int i=_values.count(); i<=index; i++) {
      field=_strSection.nextField(_ptr, _delimiters, _skipEmpty);
      if(field.isValid()) {
        if(_quotes) {
          bool beginQuote, endQuote;
          switch(field[0].unicode()) {
          case '\'':
          case '"':
            beginQuote=true;
            break;
          default:
            beginQuote=false;
            break;
          }
          switch(field[field.size()-1].unicode()) {
          case '\'':
          case '"':
            endQuote=true;
            break;
          default:
            endQuote=false;
            break;
          }
          if(beginQuote && endQuote) {
            ++field;
            field.shrink(1);
          }
        }
        _values.append(field);
      } else {
        return StringSection::null;
      }
    }
    return _values.at(index);
  }

  /*!
    Returns the first character index of column \a index.
  */
  int LineParser::startsAt(int index, bool& ok)
  {
    TRACE;
    const StringSection& v=value(index);
    if(v.isValid()) {
      return v.data()-_strSection.data();
    } else {
      ok=false;
      return 0;
    }
  }

  /*!
    Returns the last character index of column \a index.
  */
  int LineParser::endsAt(int index, bool& ok)
  {
    TRACE;
    const StringSection& v=value(index);
    if(v.isValid()) {
      return v.data()-_strSection.data()+v.size()-1;
    } else {
      ok=false;
      return 0;
    }
  }

} // namespace QGpCoreTools
