/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-12-26
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef TRACE_H
#define TRACE_H

#include "QGpCoreToolsDLLExport.h"
#include "TraceStamp.h"

namespace QGpCoreTools {

#if defined(Q_OS_WIN) || defined(Q_OS_MAC)
#define TRACE_ENABLED
#else
#define TRACE_DISABLED
#endif

#ifdef TRACE_ENABLED

#define TRACE \
  static TraceStamp _traceStamp_(__PRETTY_FUNCTION__, __LINE__); \
  Trace _trace_0(&_traceStamp_);

// Better to use ##__COUNTER__ macro but not working with gcc 4.1.3, maybe gcc 4.3
#define TRACE_N(index) \
  static TraceStamp _traceStamp_##index(__PRETTY_FUNCTION__, __LINE__); \
  Trace _trace_##index(&_traceStamp_##index);

class QGPCORETOOLS_EXPORT Trace
{
public:
  Trace(const TraceStamp * stamp);
  ~Trace();

  static QStack<const TraceStamp *> * mainStack() {return &_mainStack;}
  static QString backTrace(const QStack<const TraceStamp *> * stack);
private:
  QStack<const TraceStamp *> * _myStack;
  static QStack<const TraceStamp *> _mainStack;
};

#else
#define TRACE
#endif

} // namespace QGpCoreTools

#endif // TRACE_H
