/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2008-11-09
**  Copyright: 2008-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef XMLSAVEATTRIBUTES_H
#define XMLSAVEATTRIBUTES_H

#include <QtCore>

#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT XMLSaveAttribute : private QPair<QString,QString>
{
public:
  XMLSaveAttribute() {}
  XMLSaveAttribute(const QString& key, const QString& value) {first=key; second=value;}

  QString& key() {return first;}
  QString& value() {return second;}

  const QString& key() const {return first;}
  const QString& value() const {return second;}

  bool operator==(const XMLSaveAttribute& o) const {return first==o.first && second==o.second;}
};

class QGPCORETOOLS_EXPORT XMLSaveAttributes : private QList<XMLSaveAttribute>
{
public:
  XMLSaveAttributes() {}
  XMLSaveAttributes(const XMLSaveAttributes& o) : QList<XMLSaveAttribute>(o) {}

  inline QString& add(QString key, QString value=QString::null);

  const QString& key(int index) const {return (*this)[index].key();}
  const QString& value(int index) const {return (*this)[index].value();}
  QString& key(int index) {return (*this)[index].key();}
  QString& value(int index) {return (*this)[index].value();}
  QString value(const QString& key);

  QString toString() const;
  inline QString toEncodedString() const;

  void clear() {QList<XMLSaveAttribute>::clear();}
  bool isEmpty() const {return QList<XMLSaveAttribute>::isEmpty();}
  bool operator==(const XMLSaveAttributes& o) const {return QList<XMLSaveAttribute>::operator==(o);}
  bool isSimilar(const XMLSaveAttributes& o) const;
};

inline QString& XMLSaveAttributes::add(QString key, QString value)
{
  append(XMLSaveAttribute(key, value));
  return last().value();
}

} // namespace QGpCoreTools

#endif // XMLSAVEATTRIBUTES_H
