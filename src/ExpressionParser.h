/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2007-04-10
**  Copyright: 2007-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef EXPRESSIONPARSER_H
#define EXPRESSIONPARSER_H


#include "QGpCoreToolsDLLExport.h"
#include "ExpressionActions.h"
#include "ExpressionAction.h"
#include "Translations.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT ExpressionParser
{
  TRANSLATIONS( "ExpressionParser" );
public:
  ExpressionActions parse(QString code, ExpressionContext * context);
private:
  bool parse(ExpressionAction * parent, ExpressionContext * context);
  enum ArgumentType {NoArgument, Numeric, AlphaNumeric, String};
  bool skipArgument(ArgumentType& type);
  bool skipOperator(ExpressionAction::Type& type);
  bool skipString();
  void skipBlanks();
  void skipLineComment();
  void skipMultiLineComment();
  bool badCharacterError();
  QString bufferLocation();
private:
  const QChar * _ptr;
  int _row;
  const QChar * _beginRowPtr;
  QString _lastString;
};

} // namespace QGpCoreTools

#endif // EXPRESSIONPARSER_H
