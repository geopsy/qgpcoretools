/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2009-04-22
**  Copyright: 2009-2019
**    Marc Wathelet
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef VERSION_H
#define VERSION_H

#include "QGpCoreToolsDLLExport.h"
#include "Translations.h"

namespace QGpCoreTools {

class QGPCORETOOLS_EXPORT Version
{
  TRANSLATIONS("Version")
public:
  Version(int a=0, int b=0, int c=0, const QString& name=QString::null);
  Version(const QString& v);
  Version(const Version& v) {operator=(v);}

  void operator=(const Version& v);
  bool operator<(const Version& v) const;
  bool operator>(const Version& v) const;
  bool operator==(const Version& v) const;
  bool operator>=(const Version& v) const {return operator>(v) || operator==(v);}
  bool operator<=(const Version& v) const {return operator<(v) || operator==(v);}

  bool isDevel() const {return _a==0 && _b==0 && _c==0;}
  bool compatible(const Version& v) const;
  QString toString() const;
private:
  void parseError(const QString& v);

  int _a, _b, _c;
  QString _releaseName;
};

} // namespace QGpCoreTools

#endif // VERSION_H
