/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2004-03-04
**  Copyright: 2004-2019
**    Marc Wathelet
**    Marc Wathelet (ULg, Liège, Belgium)
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef SAMPLINGPARAMETERS_H
#define SAMPLINGPARAMETERS_H

#include "QGpCoreToolsDLLExport.h"
#include "AbstractParameters.h"

namespace QGpCoreTools {

  enum SamplingOption {LinearScale=0, LogScale=1, InversedScale=2, Interpole=4};
  Q_DECLARE_FLAGS(SamplingOptions, SamplingOption)

  class QGPCORETOOLS_EXPORT SamplingParameters: public AbstractParameters
  {
  public:
    SamplingParameters();
    SamplingParameters(const SamplingParameters& o);

    virtual AbstractParameters * clone() const {return new SamplingParameters(*this);}

    enum Type {Linear,Log};
    enum StepType {Step, Count};

    void setType(Type t) {_type=t; setCount();}
    void setType(QString t);
    Type type() const {return _type;}
    QString typeString() const;
    SamplingOptions options() const;

    void setStepType(StepType t) {_stepType=t; setCount();}
    void setStepType(QString t);
    StepType stepType() const {return _stepType;}
    QString stepTypeString() const;

    void setRange(double min, double max) {_min= min; _max=max; setCount();}
    void setMinimum(double m) {_min=m; setCount();}
    double minimum() const {return _min;}
    void setMaximum(double m) {_max=m; setCount();}
    double maximum() const {return _max;}

    void setCount(int n);
    int count() const {return _n;}

    void setStep(double step);
    double step() const {return _step;}

    bool isValid() const {return _n>1 && std::isfinite(_min) && std::isfinite(_max) && _min<_max && _step>0.0;}

    void setInversed(bool i) {_inversed=i;}
    bool isInversed() const {return _inversed;}

    double value(int i) const;
    QVector<double> values() const;

    QString toUserString() const;

    virtual QString toString(PARAMETERS_TOSTRING_ARGS_DECL) const;
    virtual void collectKeywords(PARAMETERS_COLLECTKEYWORDS_ARGS);
  protected:
    virtual int keywordCount(PARAMETERS_KEYWORDCOUNT_ARGS) const;
    virtual bool setValue(PARAMETERS_SETVALUE_ARGS);
  private:
    void setCount();

    bool _inversed;
    Type _type;
    StepType _stepType;
    int _n;
    double _min, _max, _step;
    double _roundMin;
  };

} // namespace QGpCoreTools

Q_DECLARE_OPERATORS_FOR_FLAGS(QGpCoreTools::SamplingOptions)

#endif // SAMPLINGPARAMETERS_H
