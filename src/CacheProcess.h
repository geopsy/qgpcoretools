/***************************************************************************
**
**  This file is part of QGpCoreTools.
**
**  This library is free software; you can redistribute it and/or
**  modify it under the terms of the GNU Lesser General Public
**  License as published by the Free Software Foundation; either
**  version 2.1 of the License, or (at your option) any later version.
**
**  This file is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License along with this library; if not, write to the Free Software
**  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
**  See http://www.geopsy.org for more information.
**
**  Created: 2010-09-07
**  Copyright: 2010-2019
**    Marc Wathelet (LGIT, Grenoble, France)
**
***************************************************************************/

#ifndef CACHEPROCESS_H
#define CACHEPROCESS_H

#include <QtCore>

#include "CacheItem.h"
#include "QGpCoreToolsDLLExport.h"

namespace QGpCoreTools {

  class QGPCORETOOLS_EXPORT CacheProcess
  {
  public:
    CacheProcess() {_current=0; _start=ApplicationClock::elapsed();}
    ~CacheProcess();

    inline void add(const CacheItem * item);
    inline void remove(const CacheItem * item);
    inline CacheProcess& operator<<(const CacheItem * item);
    void next() {_current++;}
    void next(int count) {_current+=count;}

    quint64 predictedTime(const CacheItem * item) const;
  private:
    QHash<const CacheItem *, int> _items;
    int _current;
    quint64 _start;
  };

  inline void CacheProcess::add(const CacheItem * item)
  {
    _items.insert(item, _items.count());
    item->addProcess(this);
  }

  inline CacheProcess& CacheProcess::operator<<(const CacheItem * item)
  {
    _items.insert(item, _items.count());
    item->addProcess(this);
    return *this;
  }

  inline void CacheProcess::remove(const CacheItem * item)
  {
    _items.remove(item);
  }

} // namespace QGpCoreTools

#endif // CACHEPROCESS_H
